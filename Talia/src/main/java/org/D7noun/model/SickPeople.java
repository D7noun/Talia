package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.D7noun.model.File.DiseaseNature;

@Entity
public class SickPeople implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	private String name;
	private String sicknessType;
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@Enumerated(EnumType.STRING)
	private DiseaseNature diseaseNature;

	private String treatmentType;
	private double monthylTreatmentPayment;

	private String helpingPartyName;
	private String helpingPartyPercentage;

	private String notes;

	@ManyToOne
	private File file;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the sicknessType
	 */
	public String getSicknessType() {
		return sicknessType;
	}

	/**
	 * @param sicknessType
	 *            the sicknessType to set
	 */
	public void setSicknessType(String sicknessType) {
		this.sicknessType = sicknessType;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the diseaseNature
	 */
	public DiseaseNature getDiseaseNature() {
		return diseaseNature;
	}

	/**
	 * @param diseaseNature
	 *            the diseaseNature to set
	 */
	public void setDiseaseNature(DiseaseNature diseaseNature) {
		this.diseaseNature = diseaseNature;
	}

	/**
	 * @return the monthylTreatmentPayment
	 */
	public double getMonthylTreatmentPayment() {
		return monthylTreatmentPayment;
	}

	/**
	 * @param monthylTreatmentPayment
	 *            the monthylTreatmentPayment to set
	 */
	public void setMonthylTreatmentPayment(double monthylTreatmentPayment) {
		this.monthylTreatmentPayment = monthylTreatmentPayment;
	}

	/**
	 * @return the helpingPartyName
	 */
	public String getHelpingPartyName() {
		return helpingPartyName;
	}

	/**
	 * @param helpingPartyName
	 *            the helpingPartyName to set
	 */
	public void setHelpingPartyName(String helpingPartyName) {
		this.helpingPartyName = helpingPartyName;
	}

	/**
	 * @return the helpingPartyPercentage
	 */
	public String getHelpingPartyPercentage() {
		return helpingPartyPercentage;
	}

	/**
	 * @param helpingPartyPercentage
	 *            the helpingPartyPercentage to set
	 */
	public void setHelpingPartyPercentage(String helpingPartyPercentage) {
		this.helpingPartyPercentage = helpingPartyPercentage;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the treatmentType
	 */
	public String getTreatmentType() {
		return treatmentType;
	}

	/**
	 * @param treatmentType
	 *            the treatmentType to set
	 */
	public void setTreatmentType(String treatmentType) {
		this.treatmentType = treatmentType;
	}

}
