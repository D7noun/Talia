package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.D7noun.model.File.SchoolType;

@Entity
public class EducationalPeople implements Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	private String name;
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;

	private String school;
	private String className;

	@Enumerated(EnumType.STRING)
	private SchoolType schoolType;

	private double annualPayment;

	private String partyHelpingName;
	private double partyHelpingContribution;

	@ManyToOne
	private File file;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the school
	 */
	public String getSchool() {
		return school;
	}

	/**
	 * @param school
	 *            the school to set
	 */
	public void setSchool(String school) {
		this.school = school;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className
	 *            the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the schoolType
	 */
	public SchoolType getSchoolType() {
		return schoolType;
	}

	/**
	 * @param schoolType
	 *            the schoolType to set
	 */
	public void setSchoolType(SchoolType schoolType) {
		this.schoolType = schoolType;
	}

	/**
	 * @return the annualPayment
	 */
	public double getAnnualPayment() {
		return annualPayment;
	}

	/**
	 * @param annualPayment
	 *            the annualPayment to set
	 */
	public void setAnnualPayment(double annualPayment) {
		this.annualPayment = annualPayment;
	}

	/**
	 * @return the partyHelpingName
	 */
	public String getPartyHelpingName() {
		return partyHelpingName;
	}

	/**
	 * @param partyHelpingName
	 *            the partyHelpingName to set
	 */
	public void setPartyHelpingName(String partyHelpingName) {
		this.partyHelpingName = partyHelpingName;
	}

	/**
	 * @return the partyHelpingContribution
	 */
	public double getPartyHelpingContribution() {
		return partyHelpingContribution;
	}

	/**
	 * @param partyHelpingContribution
	 *            the partyHelpingContribution to set
	 */
	public void setPartyHelpingContribution(double partyHelpingContribution) {
		this.partyHelpingContribution = partyHelpingContribution;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(annualPayment);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		temp = Double.doubleToLongBits(partyHelpingContribution);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((partyHelpingName == null) ? 0 : partyHelpingName.hashCode());
		result = prime * result + ((school == null) ? 0 : school.hashCode());
		result = prime * result + ((schoolType == null) ? 0 : schoolType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EducationalPeople other = (EducationalPeople) obj;
		if (Double.doubleToLongBits(annualPayment) != Double.doubleToLongBits(other.annualPayment))
			return false;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(partyHelpingContribution) != Double
				.doubleToLongBits(other.partyHelpingContribution))
			return false;
		if (partyHelpingName == null) {
			if (other.partyHelpingName != null)
				return false;
		} else if (!partyHelpingName.equals(other.partyHelpingName))
			return false;
		if (school == null) {
			if (other.school != null)
				return false;
		} else if (!school.equals(other.school))
			return false;
		if (schoolType != other.schoolType)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EducationalPeople [name=" + name + ", dateOfBirth=" + dateOfBirth + ", school=" + school
				+ ", className=" + className + ", schoolType=" + schoolType + ", annualPayment=" + annualPayment
				+ ", partyHelpingName=" + partyHelpingName + ", partyHelpingContribution=" + partyHelpingContribution
				+ ", file=" + file + "]";
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
