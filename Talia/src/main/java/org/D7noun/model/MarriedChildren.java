package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.D7noun.model.File.EducationalLevel;
import org.D7noun.model.File.SocialStatus;

@Entity
public class MarriedChildren implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	private String name;
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	private String relationType;

	@Enumerated(EnumType.STRING)
	private EducationalLevel educationalLevel;
	@Enumerated(EnumType.STRING)
	private SocialStatus socialStatus;

	private String workType;

	private double assistanceValue;

	@ManyToOne
	private File file;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the relationType
	 */
	public String getRelationType() {
		return relationType;
	}

	/**
	 * @param relationType
	 *            the relationType to set
	 */
	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	/**
	 * @return the educationalLevel
	 */
	public EducationalLevel getEducationalLevel() {
		return educationalLevel;
	}

	/**
	 * @param educationalLevel
	 *            the educationalLevel to set
	 */
	public void setEducationalLevel(EducationalLevel educationalLevel) {
		this.educationalLevel = educationalLevel;
	}

	/**
	 * @return the socialStatus
	 */
	public SocialStatus getSocialStatus() {
		return socialStatus;
	}

	/**
	 * @param socialStatus
	 *            the socialStatus to set
	 */
	public void setSocialStatus(SocialStatus socialStatus) {
		this.socialStatus = socialStatus;
	}

	/**
	 * @return the workType
	 */
	public String getWorkType() {
		return workType;
	}

	/**
	 * @param workType
	 *            the workType to set
	 */
	public void setWorkType(String workType) {
		this.workType = workType;
	}

	/**
	 * @return the assistanceValue
	 */
	public double getAssistanceValue() {
		return assistanceValue;
	}

	/**
	 * @param assistanceValue
	 *            the assistanceValue to set
	 */
	public void setAssistanceValue(double assistanceValue) {
		this.assistanceValue = assistanceValue;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(assistanceValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((educationalLevel == null) ? 0 : educationalLevel.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((relationType == null) ? 0 : relationType.hashCode());
		result = prime * result + ((socialStatus == null) ? 0 : socialStatus.hashCode());
		result = prime * result + ((workType == null) ? 0 : workType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MarriedChildren other = (MarriedChildren) obj;
		if (Double.doubleToLongBits(assistanceValue) != Double.doubleToLongBits(other.assistanceValue))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (educationalLevel != other.educationalLevel)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (relationType == null) {
			if (other.relationType != null)
				return false;
		} else if (!relationType.equals(other.relationType))
			return false;
		if (socialStatus != other.socialStatus)
			return false;
		if (workType == null) {
			if (other.workType != null)
				return false;
		} else if (!workType.equals(other.workType))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MarriedChildren [name=" + name + ", dateOfBirth=" + dateOfBirth + ", relationType=" + relationType
				+ ", educationalLevel=" + educationalLevel + ", socialStatus=" + socialStatus + ", workType=" + workType
				+ ", assistanceValue=" + assistanceValue + "]";
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
