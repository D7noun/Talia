package org.D7noun.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.D7noun.model.File.FamilyAidParty;
import org.D7noun.model.File.FamilyAidType;

@Entity
public class FamilyAid implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@Enumerated(EnumType.STRING)
	private FamilyAidParty familyAidParty;

	@Enumerated(EnumType.STRING)
	private FamilyAidType familyAidType;

	private String value;

	private String notes;

	@ManyToOne
	private File file;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the familyAidParty
	 */
	public FamilyAidParty getFamilyAidParty() {
		return familyAidParty;
	}

	/**
	 * @param familyAidParty
	 *            the familyAidParty to set
	 */
	public void setFamilyAidParty(FamilyAidParty familyAidParty) {
		this.familyAidParty = familyAidParty;
	}

	/**
	 * @return the familyAidType
	 */
	public FamilyAidType getFamilyAidType() {
		return familyAidType;
	}

	/**
	 * @param familyAidType
	 *            the familyAidType to set
	 */
	public void setFamilyAidType(FamilyAidType familyAidType) {
		this.familyAidType = familyAidType;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((familyAidParty == null) ? 0 : familyAidParty.hashCode());
		result = prime * result + ((familyAidType == null) ? 0 : familyAidType.hashCode());
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FamilyAid other = (FamilyAid) obj;
		if (familyAidParty != other.familyAidParty)
			return false;
		if (familyAidType != other.familyAidType)
			return false;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FamilyAid [id=" + id + ", familyAidParty=" + familyAidParty + ", familyAidType=" + familyAidType
				+ ", value=" + value + ", notes=" + notes + ", file=" + file + "]";
	}

}
