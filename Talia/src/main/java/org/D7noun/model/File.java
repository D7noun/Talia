package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
public class File implements Serializable {

	/**
	 * 
	 * ENUMS
	 * 
	 */

	public enum SadaAmma {
		Sada, Amma;
	}

	public enum SocialStatus {
		Married, Single, Divorced, Widowed;
	}

	public enum EducationalLevel {
		Illiterate, FirstElementary, SecondElementary, HighSchool, Graduate, Vocational, PHD;
	}

	public enum SchoolType {
		Private, Formal, Internal, External;
	}

	public enum HealthStatus {
		Alive, Sick, Dead;
	}

	public enum FamilyStatus {
		WithTheFamily, WithoutTheFamily;
	}

	public enum InsuranceType {
		Daman, TaawniyyatMowazzafin, MoaassasatDawla, InsuranceCompany, Other;
	}

	public enum DiseaseNature {
		Chronic, Handicap, Infection;
	}

	public enum ResidenceType {
		Own, Rent, Share, Publicity;
	}

	public enum ResidenceState {
		Good, Fair, Bad;
	}

	public enum BelongingType {
		House, Shop, Land, Vehicle, MotorCycle, Money, Others;
	}

	public enum BelongingBenifit {
		Renting, Planting, Investment, Others;
	}

	public enum FamilyAidParty {
		Emdad, Sayyid, Banin, Sila;
	}

	public enum FamilyAidType {
		Social, Health, Custody, Supply;
	}

	public enum Curreny {
		Lira, Dollar;
	}

	public enum PaymentWay {
		Monthly, Annually;
	}

	public enum FinancialAidWay {
		Fixed, NotFixed;
	}

	public enum FinancialAidType {
		SahemImam, SahemSeda, Aytam, Sadakat;
	}

	public enum HealthAidType {
		Medicine, Hospital, Treatment;
	}

	public enum AttachmentType {
		Dead, Divorce, Family, FirstBenefit, Husband, Medical, Medicine;
	}

	/**
	 * 
	 * FIELDS
	 * 
	 */

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * Personal Information
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id; // applicationNumber

	@Version
	@Column(name = "version")
	private int version;

	@Temporal(TemporalType.DATE)
	private Date applicationSubmissionDate;

	@Temporal(TemporalType.DATE)
	private Date familyVisitDate;

	// CODE
	private String caseType;

	private String applicationType;

	private String firstPhoneNumber;
	private String secondPhoneNumber;

	private String husbandName;
	private String wifeName;

	private String emailAddress;

	private String homeAddress;

	private String inspector;
	private String inspectingSource;

	/**
	 * 
	 * Husband Personal Information
	 * 
	 */

	private String husbandFirstName;
	private String husbandFatherName;
	private String husbandFamilyName;

	private String husbandBirthPlace;
	@Temporal(TemporalType.DATE)
	private Date husbandBirthdate;

	private int husbandRecordNumber;

	private String husbandMotherFullName;

	private String husbandGovernorate;
	private String husbandProvince;
	private String husbandTown;
	private String husbandNationality;

	@Enumerated(EnumType.STRING)
	private SadaAmma husbandSadaAmma;

	@Enumerated(EnumType.STRING)
	private SocialStatus husbandSocialStatus;

	private int husbandNumberOfWife;

	@Enumerated(EnumType.STRING)
	private EducationalLevel husbandEducationalLevel;
	private String husbandEducationalCertificate;

	@Enumerated(EnumType.STRING)
	private HealthStatus husbandHealthStatus;

	@Temporal(TemporalType.DATE)
	private Date husbandDeathDate;

	private String husbandDeathReason;

	@Enumerated(EnumType.STRING)
	private FamilyStatus husbandFamilyStatus;

	private String husbandNotes;

	/**
	 * 
	 * Husband Job Information
	 * 
	 */

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<HusbandWork> husbandWorks = new HashSet<HusbandWork>();

	@Enumerated(EnumType.STRING)
	private InsuranceType husbandInsuranceType;

	private boolean husbandIndemnity;
	private double husbandIndemnityPrice;
	@Temporal(TemporalType.DATE)
	private Date husbandIndemnityDate;

	private boolean husbandRetirement;
	private double husbandRetirementSalary;
	private String husbandRetirementNotes;

	private String husbandInspectingNotes;

	/**
	 * 
	 * Wife Personal Information
	 * 
	 */

	private String wifeFirstName;
	private String wifeFatherName;
	private String wifeFamilyName;

	@Temporal(TemporalType.DATE)
	private Date wifeBirthDate;
	private String wifeBirthPlace;

	private int wifeRecordNumber;

	private String wifeMotherFullName;
	private String wifeGovernorate;
	private String wifeProvince;
	private String wifeTown;
	private String wifeNationality;

	@Enumerated(EnumType.STRING)
	private SadaAmma wifeSadaAmma;
	@Enumerated(EnumType.STRING)
	private EducationalLevel wifeEducationalLevel;
	@Enumerated(EnumType.STRING)
	private HealthStatus wifehealthStatus;
	@Enumerated(EnumType.STRING)
	private FamilyStatus wifeFamilyStatus;

	private boolean wifeMarriedAgain;

	private String wifeFamilyResponsibility;
	private double wifeResponsibleContribution;

	private String wifeExHusbandName;
	private String wifeExHusbandJob;
	private String wifeExHusbandStatus;

	/**
	 * 
	 * Wife Job Information
	 * 
	 */

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<WifeWork> wifeWorks = new HashSet<WifeWork>();

	private boolean wifeCurrentlyWorking;
	private boolean wifeRequestWorking;
	private String wifePreferedWork;

	private String wifeWorkType;
	private String wifeWorkAddress;
	private String wifeWorkPhoneNumber;
	@Temporal(TemporalType.DATE)
	private Date wifeWorkStartDate;
	private double wifeWorkSalary;

	private String wifeInspectingNotes;

	/**
	 * 
	 * People living with the family
	 * 
	 */

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<RelativeFamilyPeople> relativefamilyPeoples = new HashSet<RelativeFamilyPeople>();
	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<OtherFamilyPeople> otherFamilyPeoples = new HashSet<OtherFamilyPeople>();

	/**
	 * 
	 * Information about the people of this family
	 * 
	 */

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<MarriedChildren> marriedChildrens = new HashSet<MarriedChildren>();

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<EducationalPeople> educationalPeoples = new HashSet<EducationalPeople>();

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<SickPeople> sickPeoples = new HashSet<SickPeople>();

	private String familyPeopleInspectingNotes;

	/**
	 * 
	 * Working People inside the family
	 * 
	 */

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<WorkingPeople> workingPeoples = new HashSet<WorkingPeople>();

	/**
	 * 
	 * Residence
	 * 
	 */

	@Enumerated(EnumType.STRING)
	private ResidenceType residenceType;

	@Enumerated(EnumType.STRING)
	private ResidenceState residenceState;

	private int numberOfRooms;

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Furniture> furnitures = new HashSet<Furniture>();

	private String residenceInspectingNotes;

	/**
	 * 
	 * Belongings
	 * 
	 */

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Belonging> belongings = new HashSet<Belonging>();

	private String belongingInspectingNotes;

	/**
	 * 
	 * Aid, Income, OutCome
	 * 
	 */

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<FamilyAid> familyAids = new HashSet<FamilyAid>();

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<FamilyIncome> familyIncomes = new HashSet<FamilyIncome>();

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<FamilyOutcome> familyOutcomes = new HashSet<FamilyOutcome>();

	private String financialInspectingNotes;

	/**
	 * 
	 * Notess
	 * 
	 */

	private String socialSectionNotes;

	/**
	 * 
	 * Recommended Aids
	 * 
	 */

	private boolean aidFinancial;
	@Enumerated(EnumType.STRING)
	private FinancialAidWay aidFinancialWay;
	@Enumerated(EnumType.STRING)
	private FinancialAidType aidFinancialType;

	private double aidFinancialValue;

	// ////////////////////

	private boolean aidFood;
	private String aidFoodNotes;

	// ////////////////////

	private boolean aidHealth;
	@Enumerated(EnumType.STRING)
	private HealthAidType aidHealthType;

	private String aidHealthNotes;

	// ////////////////////

	private boolean aidProjectOwning;
	private String aidProjectType;
	private double aidProjectValue;

	/**
	 * 
	 * Attachments;
	 * 
	 */

	@OneToMany(mappedBy = "file", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<Attachment> attachments = new HashSet<Attachment>();

	/**
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (id != null)
			result += "id: " + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof File)) {
			return false;
		}
		File other = (File) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the applicationSubmissionDate
	 */
	public Date getApplicationSubmissionDate() {
		return applicationSubmissionDate;
	}

	/**
	 * @param applicationSubmissionDate
	 *            the applicationSubmissionDate to set
	 */
	public void setApplicationSubmissionDate(Date applicationSubmissionDate) {
		this.applicationSubmissionDate = applicationSubmissionDate;
	}

	/**
	 * @return the familyVisitDate
	 */
	public Date getFamilyVisitDate() {
		return familyVisitDate;
	}

	/**
	 * @param familyVisitDate
	 *            the familyVisitDate to set
	 */
	public void setFamilyVisitDate(Date familyVisitDate) {
		this.familyVisitDate = familyVisitDate;
	}

	/**
	 * @return the caseType
	 */
	public String getCaseType() {
		return caseType;
	}

	/**
	 * @param caseType
	 *            the caseType to set
	 */
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	/**
	 * @return the applicationType
	 */
	public String getApplicationType() {
		return applicationType;
	}

	/**
	 * @param applicationType
	 *            the applicationType to set
	 */
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	/**
	 * @return the firstPhoneNumber
	 */
	public String getFirstPhoneNumber() {
		return firstPhoneNumber;
	}

	/**
	 * @param firstPhoneNumber
	 *            the firstPhoneNumber to set
	 */
	public void setFirstPhoneNumber(String firstPhoneNumber) {
		this.firstPhoneNumber = firstPhoneNumber;
	}

	/**
	 * @return the secondPhoneNumber
	 */
	public String getSecondPhoneNumber() {
		return secondPhoneNumber;
	}

	/**
	 * @param secondPhoneNumber
	 *            the secondPhoneNumber to set
	 */
	public void setSecondPhoneNumber(String secondPhoneNumber) {
		this.secondPhoneNumber = secondPhoneNumber;
	}

	/**
	 * @return the husbandName
	 */
	public String getHusbandName() {
		return husbandName;
	}

	/**
	 * @param husbandName
	 *            the husbandName to set
	 */
	public void setHusbandName(String husbandName) {
		this.husbandName = husbandName;
	}

	/**
	 * @return the wifeName
	 */
	public String getWifeName() {
		return wifeName;
	}

	/**
	 * @param wifeName
	 *            the wifeName to set
	 */
	public void setWifeName(String wifeName) {
		this.wifeName = wifeName;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the homeAddress
	 */
	public String getHomeAddress() {
		return homeAddress;
	}

	/**
	 * @param homeAddress
	 *            the homeAddress to set
	 */
	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}

	/**
	 * @return the inspector
	 */
	public String getInspector() {
		return inspector;
	}

	/**
	 * @param inspector
	 *            the inspector to set
	 */
	public void setInspector(String inspector) {
		this.inspector = inspector;
	}

	/**
	 * @return the inspectingSource
	 */
	public String getInspectingSource() {
		return inspectingSource;
	}

	/**
	 * @param inspectingSource
	 *            the inspectingSource to set
	 */
	public void setInspectingSource(String inspectingSource) {
		this.inspectingSource = inspectingSource;
	}

	/**
	 * @return the husbandFirstName
	 */
	public String getHusbandFirstName() {
		return husbandFirstName;
	}

	/**
	 * @param husbandFirstName
	 *            the husbandFirstName to set
	 */
	public void setHusbandFirstName(String husbandFirstName) {
		this.husbandFirstName = husbandFirstName;
	}

	/**
	 * @return the husbandFatherName
	 */
	public String getHusbandFatherName() {
		return husbandFatherName;
	}

	/**
	 * @param husbandFatherName
	 *            the husbandFatherName to set
	 */
	public void setHusbandFatherName(String husbandFatherName) {
		this.husbandFatherName = husbandFatherName;
	}

	/**
	 * @return the husbandFamilyName
	 */
	public String getHusbandFamilyName() {
		return husbandFamilyName;
	}

	/**
	 * @param husbandFamilyName
	 *            the husbandFamilyName to set
	 */
	public void setHusbandFamilyName(String husbandFamilyName) {
		this.husbandFamilyName = husbandFamilyName;
	}

	/**
	 * @return the husbandBirthPlace
	 */
	public String getHusbandBirthPlace() {
		return husbandBirthPlace;
	}

	/**
	 * @param husbandBirthPlace
	 *            the husbandBirthPlace to set
	 */
	public void setHusbandBirthPlace(String husbandBirthPlace) {
		this.husbandBirthPlace = husbandBirthPlace;
	}

	/**
	 * @return the husbandBirthdate
	 */
	public Date getHusbandBirthdate() {
		return husbandBirthdate;
	}

	/**
	 * @param husbandBirthdate
	 *            the husbandBirthdate to set
	 */
	public void setHusbandBirthdate(Date husbandBirthdate) {
		this.husbandBirthdate = husbandBirthdate;
	}

	/**
	 * @return the husbandRecordNumber
	 */
	public int getHusbandRecordNumber() {
		return husbandRecordNumber;
	}

	/**
	 * @param husbandRecordNumber
	 *            the husbandRecordNumber to set
	 */
	public void setHusbandRecordNumber(int husbandRecordNumber) {
		this.husbandRecordNumber = husbandRecordNumber;
	}

	/**
	 * @return the husbandMotherFullName
	 */
	public String getHusbandMotherFullName() {
		return husbandMotherFullName;
	}

	/**
	 * @param husbandMotherFullName
	 *            the husbandMotherFullName to set
	 */
	public void setHusbandMotherFullName(String husbandMotherFullName) {
		this.husbandMotherFullName = husbandMotherFullName;
	}

	/**
	 * @return the husbandGovernorate
	 */
	public String getHusbandGovernorate() {
		return husbandGovernorate;
	}

	/**
	 * @param husbandGovernorate
	 *            the husbandGovernorate to set
	 */
	public void setHusbandGovernorate(String husbandGovernorate) {
		this.husbandGovernorate = husbandGovernorate;
	}

	/**
	 * @return the husbandProvince
	 */
	public String getHusbandProvince() {
		return husbandProvince;
	}

	/**
	 * @param husbandProvince
	 *            the husbandProvince to set
	 */
	public void setHusbandProvince(String husbandProvince) {
		this.husbandProvince = husbandProvince;
	}

	/**
	 * @return the husbandTown
	 */
	public String getHusbandTown() {
		return husbandTown;
	}

	/**
	 * @param husbandTown
	 *            the husbandTown to set
	 */
	public void setHusbandTown(String husbandTown) {
		this.husbandTown = husbandTown;
	}

	/**
	 * @return the husbandNationality
	 */
	public String getHusbandNationality() {
		return husbandNationality;
	}

	/**
	 * @param husbandNationality
	 *            the husbandNationality to set
	 */
	public void setHusbandNationality(String husbandNationality) {
		this.husbandNationality = husbandNationality;
	}

	/**
	 * @return the husbandSadaAmma
	 */
	public SadaAmma getHusbandSadaAmma() {
		return husbandSadaAmma;
	}

	/**
	 * @param husbandSadaAmma
	 *            the husbandSadaAmma to set
	 */
	public void setHusbandSadaAmma(SadaAmma husbandSadaAmma) {
		this.husbandSadaAmma = husbandSadaAmma;
	}

	/**
	 * @return the husbandSocialStatus
	 */
	public SocialStatus getHusbandSocialStatus() {
		return husbandSocialStatus;
	}

	/**
	 * @param husbandSocialStatus
	 *            the husbandSocialStatus to set
	 */
	public void setHusbandSocialStatus(SocialStatus husbandSocialStatus) {
		this.husbandSocialStatus = husbandSocialStatus;
	}

	/**
	 * @return the husbandNumberOfWife
	 */
	public int getHusbandNumberOfWife() {
		return husbandNumberOfWife;
	}

	/**
	 * @param husbandNumberOfWife
	 *            the husbandNumberOfWife to set
	 */
	public void setHusbandNumberOfWife(int husbandNumberOfWife) {
		this.husbandNumberOfWife = husbandNumberOfWife;
	}

	/**
	 * @return the husbandEducationalLevel
	 */
	public EducationalLevel getHusbandEducationalLevel() {
		return husbandEducationalLevel;
	}

	/**
	 * @param husbandEducationalLevel
	 *            the husbandEducationalLevel to set
	 */
	public void setHusbandEducationalLevel(EducationalLevel husbandEducationalLevel) {
		this.husbandEducationalLevel = husbandEducationalLevel;
	}

	/**
	 * @return the husbandEducationalCertificate
	 */
	public String husbandDeathReason() {
		return husbandEducationalCertificate;
	}

	/**
	 * @param husbandEducationalCertificate
	 *            the husbandEducationalCertificate to set
	 */
	public void setHusbandEducationalCertificate(String husbandEducationalCertificate) {
		this.husbandEducationalCertificate = husbandEducationalCertificate;
	}

	/**
	 * @return the husbandHealthStatus
	 */
	public HealthStatus getHusbandHealthStatus() {
		return husbandHealthStatus;
	}

	/**
	 * @param husbandHealthStatus
	 *            the husbandHealthStatus to set
	 */
	public void setHusbandHealthStatus(HealthStatus husbandHealthStatus) {
		this.husbandHealthStatus = husbandHealthStatus;
	}

	/**
	 * @return the husbandDeathDate
	 */
	public Date getHusbandDeathDate() {
		return husbandDeathDate;
	}

	/**
	 * @param husbandDeathDate
	 *            the husbandDeathDate to set
	 */
	public void setHusbandDeathDate(Date husbandDeathDate) {
		this.husbandDeathDate = husbandDeathDate;
	}

	/**
	 * @return the husbandDeathReason
	 */
	public String getHusbandDeathReason() {
		return husbandDeathReason;
	}

	/**
	 * @param husbandDeathReason
	 *            the husbandDeathReason to set
	 */
	public void setHusbandDeathReason(String husbandDeathReason) {
		this.husbandDeathReason = husbandDeathReason;
	}

	/**
	 * @return the husbandFamilyStatus
	 */
	public FamilyStatus getHusbandFamilyStatus() {
		return husbandFamilyStatus;
	}

	/**
	 * @param husbandFamilyStatus
	 *            the husbandFamilyStatus to set
	 */
	public void setHusbandFamilyStatus(FamilyStatus husbandFamilyStatus) {
		this.husbandFamilyStatus = husbandFamilyStatus;
	}

	/**
	 * @return the husbandNotes
	 */
	public String getHusbandNotes() {
		return husbandNotes;
	}

	/**
	 * @param husbandNotes
	 *            the husbandNotes to set
	 */
	public void setHusbandNotes(String husbandNotes) {
		this.husbandNotes = husbandNotes;
	}

	/**
	 * @return the husbandWorks
	 */
	public Set<HusbandWork> getHusbandWorks() {
		return husbandWorks;
	}

	/**
	 * @param husbandWorks
	 *            the husbandWorks to set
	 */
	public void setHusbandWorks(Set<HusbandWork> husbandWorks) {
		this.husbandWorks = husbandWorks;
	}

	/**
	 * @return the husbandInsuranceType
	 */
	public InsuranceType getHusbandInsuranceType() {
		return husbandInsuranceType;
	}

	/**
	 * @param husbandInsuranceType
	 *            the husbandInsuranceType to set
	 */
	public void setHusbandInsuranceType(InsuranceType husbandInsuranceType) {
		this.husbandInsuranceType = husbandInsuranceType;
	}

	/**
	 * @return the husbandIndemnity
	 */
	public boolean isHusbandIndemnity() {
		return husbandIndemnity;
	}

	/**
	 * @param husbandIndemnity
	 *            the husbandIndemnity to set
	 */
	public void setHusbandIndemnity(boolean husbandIndemnity) {
		this.husbandIndemnity = husbandIndemnity;
	}

	/**
	 * @return the husbandIndemnityPrice
	 */
	public double getHusbandIndemnityPrice() {
		return husbandIndemnityPrice;
	}

	/**
	 * @param husbandIndemnityPrice
	 *            the husbandIndemnityPrice to set
	 */
	public void setHusbandIndemnityPrice(double husbandIndemnityPrice) {
		this.husbandIndemnityPrice = husbandIndemnityPrice;
	}

	/**
	 * @return the husbandIndemnityDate
	 */
	public Date getHusbandIndemnityDate() {
		return husbandIndemnityDate;
	}

	/**
	 * @param husbandIndemnityDate
	 *            the husbandIndemnityDate to set
	 */
	public void setHusbandIndemnityDate(Date husbandIndemnityDate) {
		this.husbandIndemnityDate = husbandIndemnityDate;
	}

	/**
	 * @return the husbandRetirement
	 */
	public boolean isHusbandRetirement() {
		return husbandRetirement;
	}

	/**
	 * @param husbandRetirement
	 *            the husbandRetirement to set
	 */
	public void setHusbandRetirement(boolean husbandRetirement) {
		this.husbandRetirement = husbandRetirement;
	}

	/**
	 * @return the husbandRetirementSalary
	 */
	public double getHusbandRetirementSalary() {
		return husbandRetirementSalary;
	}

	/**
	 * @param husbandRetirementSalary
	 *            the husbandRetirementSalary to set
	 */
	public void setHusbandRetirementSalary(double husbandRetirementSalary) {
		this.husbandRetirementSalary = husbandRetirementSalary;
	}

	/**
	 * @return the husbandRetirementNotes
	 */
	public String getHusbandRetirementNotes() {
		return husbandRetirementNotes;
	}

	/**
	 * @param husbandRetirementNotes
	 *            the husbandRetirementNotes to set
	 */
	public void setHusbandRetirementNotes(String husbandRetirementNotes) {
		this.husbandRetirementNotes = husbandRetirementNotes;
	}

	/**
	 * @return the husbandInspectingNotes
	 */
	public String getHusbandInspectingNotes() {
		return husbandInspectingNotes;
	}

	/**
	 * @param husbandInspectingNotes
	 *            the husbandInspectingNotes to set
	 */
	public void setHusbandInspectingNotes(String husbandInspectingNotes) {
		this.husbandInspectingNotes = husbandInspectingNotes;
	}

	/**
	 * @return the wifeFirstName
	 */
	public String getWifeFirstName() {
		return wifeFirstName;
	}

	/**
	 * @param wifeFirstName
	 *            the wifeFirstName to set
	 */
	public void setWifeFirstName(String wifeFirstName) {
		this.wifeFirstName = wifeFirstName;
	}

	/**
	 * @return the wifeFatherName
	 */
	public String getWifeFatherName() {
		return wifeFatherName;
	}

	/**
	 * @param wifeFatherName
	 *            the wifeFatherName to set
	 */
	public void setWifeFatherName(String wifeFatherName) {
		this.wifeFatherName = wifeFatherName;
	}

	/**
	 * @return the wifeFamilyName
	 */
	public String getWifeFamilyName() {
		return wifeFamilyName;
	}

	/**
	 * @param wifeFamilyName
	 *            the wifeFamilyName to set
	 */
	public void setWifeFamilyName(String wifeFamilyName) {
		this.wifeFamilyName = wifeFamilyName;
	}

	/**
	 * @return the wifeBirthDate
	 */
	public Date getWifeBirthDate() {
		return wifeBirthDate;
	}

	/**
	 * @param wifeBirthDate
	 *            the wifeBirthDate to set
	 */
	public void setWifeBirthDate(Date wifeBirthDate) {
		this.wifeBirthDate = wifeBirthDate;
	}

	/**
	 * @return the wifeBirthPlace
	 */
	public String getWifeBirthPlace() {
		return wifeBirthPlace;
	}

	/**
	 * @param wifeBirthPlace
	 *            the wifeBirthPlace to set
	 */
	public void setWifeBirthPlace(String wifeBirthPlace) {
		this.wifeBirthPlace = wifeBirthPlace;
	}

	/**
	 * @return the wifeRecordNumber
	 */
	public int getWifeRecordNumber() {
		return wifeRecordNumber;
	}

	/**
	 * @param wifeRecordNumber
	 *            the wifeRecordNumber to set
	 */
	public void setWifeRecordNumber(int wifeRecordNumber) {
		this.wifeRecordNumber = wifeRecordNumber;
	}

	/**
	 * @return the wifeMotherFullName
	 */
	public String getWifeMotherFullName() {
		return wifeMotherFullName;
	}

	/**
	 * @param wifeMotherFullName
	 *            the wifeMotherFullName to set
	 */
	public void setWifeMotherFullName(String wifeMotherFullName) {
		this.wifeMotherFullName = wifeMotherFullName;
	}

	/**
	 * @return the wifeGovernorate
	 */
	public String getWifeGovernorate() {
		return wifeGovernorate;
	}

	/**
	 * @param wifeGovernorate
	 *            the wifeGovernorate to set
	 */
	public void setWifeGovernorate(String wifeGovernorate) {
		this.wifeGovernorate = wifeGovernorate;
	}

	/**
	 * @return the wifeProvince
	 */
	public String getWifeProvince() {
		return wifeProvince;
	}

	/**
	 * @param wifeProvince
	 *            the wifeProvince to set
	 */
	public void setWifeProvince(String wifeProvince) {
		this.wifeProvince = wifeProvince;
	}

	/**
	 * @return the wifeTown
	 */
	public String getWifeTown() {
		return wifeTown;
	}

	/**
	 * @param wifeTown
	 *            the wifeTown to set
	 */
	public void setWifeTown(String wifeTown) {
		this.wifeTown = wifeTown;
	}

	/**
	 * @return the wifeNationality
	 */
	public String getWifeNationality() {
		return wifeNationality;
	}

	/**
	 * @param wifeNationality
	 *            the wifeNationality to set
	 */
	public void setWifeNationality(String wifeNationality) {
		this.wifeNationality = wifeNationality;
	}

	/**
	 * @return the wifeSadaAmma
	 */
	public SadaAmma getWifeSadaAmma() {
		return wifeSadaAmma;
	}

	/**
	 * @param wifeSadaAmma
	 *            the wifeSadaAmma to set
	 */
	public void setWifeSadaAmma(SadaAmma wifeSadaAmma) {
		this.wifeSadaAmma = wifeSadaAmma;
	}

	/**
	 * @return the wifeEducationalLevel
	 */
	public EducationalLevel getWifeEducationalLevel() {
		return wifeEducationalLevel;
	}

	/**
	 * @param wifeEducationalLevel
	 *            the wifeEducationalLevel to set
	 */
	public void setWifeEducationalLevel(EducationalLevel wifeEducationalLevel) {
		this.wifeEducationalLevel = wifeEducationalLevel;
	}

	/**
	 * @return the wifehealthStatus
	 */
	public HealthStatus getWifehealthStatus() {
		return wifehealthStatus;
	}

	/**
	 * @param wifehealthStatus
	 *            the wifehealthStatus to set
	 */
	public void setWifehealthStatus(HealthStatus wifehealthStatus) {
		this.wifehealthStatus = wifehealthStatus;
	}

	/**
	 * @return the wifeFamilyStatus
	 */
	public FamilyStatus getWifeFamilyStatus() {
		return wifeFamilyStatus;
	}

	/**
	 * @param wifeFamilyStatus
	 *            the wifeFamilyStatus to set
	 */
	public void setWifeFamilyStatus(FamilyStatus wifeFamilyStatus) {
		this.wifeFamilyStatus = wifeFamilyStatus;
	}

	/**
	 * @return the wifeMarriedAgain
	 */
	public boolean isWifeMarriedAgain() {
		return wifeMarriedAgain;
	}

	/**
	 * @param wifeMarriedAgain
	 *            the wifeMarriedAgain to set
	 */
	public void setWifeMarriedAgain(boolean wifeMarriedAgain) {
		this.wifeMarriedAgain = wifeMarriedAgain;
	}

	/**
	 * @return the wifeFamilyResponsibility
	 */
	public String getWifeFamilyResponsibility() {
		return wifeFamilyResponsibility;
	}

	/**
	 * @param wifeFamilyResponsibility
	 *            the wifeFamilyResponsibility to set
	 */
	public void setWifeFamilyResponsibility(String wifeFamilyResponsibility) {
		this.wifeFamilyResponsibility = wifeFamilyResponsibility;
	}

	/**
	 * @return the wifeResponsibleContribution
	 */
	public double getWifeResponsibleContribution() {
		return wifeResponsibleContribution;
	}

	/**
	 * @param wifeResponsibleContribution
	 *            the wifeResponsibleContribution to set
	 */
	public void setWifeResponsibleContribution(double wifeResponsibleContribution) {
		this.wifeResponsibleContribution = wifeResponsibleContribution;
	}

	/**
	 * @return the wifeExHusbandName
	 */
	public String getWifeExHusbandName() {
		return wifeExHusbandName;
	}

	/**
	 * @param wifeExHusbandName
	 *            the wifeExHusbandName to set
	 */
	public void setWifeExHusbandName(String wifeExHusbandName) {
		this.wifeExHusbandName = wifeExHusbandName;
	}

	/**
	 * @return the wifeExHusbandJob
	 */
	public String getWifeExHusbandJob() {
		return wifeExHusbandJob;
	}

	/**
	 * @param wifeExHusbandJob
	 *            the wifeExHusbandJob to set
	 */
	public void setWifeExHusbandJob(String wifeExHusbandJob) {
		this.wifeExHusbandJob = wifeExHusbandJob;
	}

	/**
	 * @return the wifeExHusbandStatus
	 */
	public String getWifeExHusbandStatus() {
		return wifeExHusbandStatus;
	}

	/**
	 * @param wifeExHusbandStatus
	 *            the wifeExHusbandStatus to set
	 */
	public void setWifeExHusbandStatus(String wifeExHusbandStatus) {
		this.wifeExHusbandStatus = wifeExHusbandStatus;
	}

	/**
	 * @return the wifeWorks
	 */
	public Set<WifeWork> getWifeWorks() {
		return wifeWorks;
	}

	/**
	 * @param wifeWorks
	 *            the wifeWorks to set
	 */
	public void setWifeWorks(Set<WifeWork> wifeWorks) {
		this.wifeWorks = wifeWorks;
	}

	/**
	 * @return the wifeCurrentlyWorking
	 */
	public boolean isWifeCurrentlyWorking() {
		return wifeCurrentlyWorking;
	}

	/**
	 * @param wifeCurrentlyWorking
	 *            the wifeCurrentlyWorking to set
	 */
	public void setWifeCurrentlyWorking(boolean wifeCurrentlyWorking) {
		this.wifeCurrentlyWorking = wifeCurrentlyWorking;
	}

	/**
	 * @return the wifeRequestWorking
	 */
	public boolean isWifeRequestWorking() {
		return wifeRequestWorking;
	}

	/**
	 * @param wifeRequestWorking
	 *            the wifeRequestWorking to set
	 */
	public void setWifeRequestWorking(boolean wifeRequestWorking) {
		this.wifeRequestWorking = wifeRequestWorking;
	}

	/**
	 * @return the wifePreferedWork
	 */
	public String getWifePreferedWork() {
		return wifePreferedWork;
	}

	/**
	 * @param wifePreferedWork
	 *            the wifePreferedWork to set
	 */
	public void setWifePreferedWork(String wifePreferedWork) {
		this.wifePreferedWork = wifePreferedWork;
	}

	/**
	 * @return the wifeWorkType
	 */
	public String getWifeWorkType() {
		return wifeWorkType;
	}

	/**
	 * @param wifeWorkType
	 *            the wifeWorkType to set
	 */
	public void setWifeWorkType(String wifeWorkType) {
		this.wifeWorkType = wifeWorkType;
	}

	/**
	 * @return the wifeWorkAddress
	 */
	public String getWifeWorkAddress() {
		return wifeWorkAddress;
	}

	/**
	 * @param wifeWorkAddress
	 *            the wifeWorkAddress to set
	 */
	public void setWifeWorkAddress(String wifeWorkAddress) {
		this.wifeWorkAddress = wifeWorkAddress;
	}

	/**
	 * @return the wifeWorkPhoneNumber
	 */
	public String getWifeWorkPhoneNumber() {
		return wifeWorkPhoneNumber;
	}

	/**
	 * @param wifeWorkPhoneNumber
	 *            the wifeWorkPhoneNumber to set
	 */
	public void setWifeWorkPhoneNumber(String wifeWorkPhoneNumber) {
		this.wifeWorkPhoneNumber = wifeWorkPhoneNumber;
	}

	/**
	 * @return the wifeWorkStartDate
	 */
	public Date getWifeWorkStartDate() {
		return wifeWorkStartDate;
	}

	/**
	 * @param wifeWorkStartDate
	 *            the wifeWorkStartDate to set
	 */
	public void setWifeWorkStartDate(Date wifeWorkStartDate) {
		this.wifeWorkStartDate = wifeWorkStartDate;
	}

	/**
	 * @return the wifeWorkSalary
	 */
	public double getWifeWorkSalary() {
		return wifeWorkSalary;
	}

	/**
	 * @param wifeWorkSalary
	 *            the wifeWorkSalary to set
	 */
	public void setWifeWorkSalary(double wifeWorkSalary) {
		this.wifeWorkSalary = wifeWorkSalary;
	}

	/**
	 * @return the wifeInspectingNotes
	 */
	public String getWifeInspectingNotes() {
		return wifeInspectingNotes;
	}

	/**
	 * @param wifeInspectingNotes
	 *            the wifeInspectingNotes to set
	 */
	public void setWifeInspectingNotes(String wifeInspectingNotes) {
		this.wifeInspectingNotes = wifeInspectingNotes;
	}

	/**
	 * @return the marriedChildrens
	 */
	public Set<MarriedChildren> getMarriedChildrens() {
		return marriedChildrens;
	}

	/**
	 * @param marriedChildrens
	 *            the marriedChildrens to set
	 */
	public void setMarriedChildrens(Set<MarriedChildren> marriedChildrens) {
		this.marriedChildrens = marriedChildrens;
	}

	/**
	 * @return the educationalPeoples
	 */
	public Set<EducationalPeople> getEducationalPeoples() {
		return educationalPeoples;
	}

	/**
	 * @param educationalPeoples
	 *            the educationalPeoples to set
	 */
	public void setEducationalPeoples(Set<EducationalPeople> educationalPeoples) {
		this.educationalPeoples = educationalPeoples;
	}

	/**
	 * @return the sickPeoples
	 */
	public Set<SickPeople> getSickPeoples() {
		return sickPeoples;
	}

	/**
	 * @param sickPeoples
	 *            the sickPeoples to set
	 */
	public void setSickPeoples(Set<SickPeople> sickPeoples) {
		this.sickPeoples = sickPeoples;
	}

	/**
	 * @return the familyPeopleInspectingNotes
	 */
	public String getFamilyPeopleInspectingNotes() {
		return familyPeopleInspectingNotes;
	}

	/**
	 * @param familyPeopleInspectingNotes
	 *            the familyPeopleInspectingNotes to set
	 */
	public void setFamilyPeopleInspectingNotes(String familyPeopleInspectingNotes) {
		this.familyPeopleInspectingNotes = familyPeopleInspectingNotes;
	}

	/**
	 * @return the workingPeoples
	 */
	public Set<WorkingPeople> getWorkingPeoples() {
		return workingPeoples;
	}

	/**
	 * @param workingPeoples
	 *            the workingPeoples to set
	 */
	public void setWorkingPeoples(Set<WorkingPeople> workingPeoples) {
		this.workingPeoples = workingPeoples;
	}

	/**
	 * @return the residenceType
	 */
	public ResidenceType getResidenceType() {
		return residenceType;
	}

	/**
	 * @param residenceType
	 *            the residenceType to set
	 */
	public void setResidenceType(ResidenceType residenceType) {
		this.residenceType = residenceType;
	}

	/**
	 * @return the residenceState
	 */
	public ResidenceState getResidenceState() {
		return residenceState;
	}

	/**
	 * @param residenceState
	 *            the residenceState to set
	 */
	public void setResidenceState(ResidenceState residenceState) {
		this.residenceState = residenceState;
	}

	/**
	 * @return the numberOfRooms
	 */
	public int getNumberOfRooms() {
		return numberOfRooms;
	}

	/**
	 * @param numberOfRooms
	 *            the numberOfRooms to set
	 */
	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}

	/**
	 * @return the furnitures
	 */
	public Set<Furniture> getFurnitures() {
		return furnitures;
	}

	/**
	 * @param furnitures
	 *            the furnitures to set
	 */
	public void setFurnitures(Set<Furniture> furnitures) {
		this.furnitures = furnitures;
	}

	/**
	 * @return the residenceInspectingNotes
	 */
	public String getResidenceInspectingNotes() {
		return residenceInspectingNotes;
	}

	/**
	 * @param residenceInspectingNotes
	 *            the residenceInspectingNotes to set
	 */
	public void setResidenceInspectingNotes(String residenceInspectingNotes) {
		this.residenceInspectingNotes = residenceInspectingNotes;
	}

	/**
	 * @return the belongings
	 */
	public Set<Belonging> getBelongings() {
		return belongings;
	}

	/**
	 * @param belongings
	 *            the belongings to set
	 */
	public void setBelongings(Set<Belonging> belongings) {
		this.belongings = belongings;
	}

	/**
	 * @return the belongingInspectingNotes
	 */
	public String getBelongingInspectingNotes() {
		return belongingInspectingNotes;
	}

	/**
	 * @param belongingInspectingNotes
	 *            the belongingInspectingNotes to set
	 */
	public void setBelongingInspectingNotes(String belongingInspectingNotes) {
		this.belongingInspectingNotes = belongingInspectingNotes;
	}

	/**
	 * @return the familyAids
	 */
	public Set<FamilyAid> getFamilyAids() {
		return familyAids;
	}

	/**
	 * @param familyAids
	 *            the familyAids to set
	 */
	public void setFamilyAids(Set<FamilyAid> familyAids) {
		this.familyAids = familyAids;
	}

	/**
	 * @return the familyIncomes
	 */
	public Set<FamilyIncome> getFamilyIncomes() {
		return familyIncomes;
	}

	/**
	 * @param familyIncomes
	 *            the familyIncomes to set
	 */
	public void setFamilyIncomes(Set<FamilyIncome> familyIncomes) {
		this.familyIncomes = familyIncomes;
	}

	/**
	 * @return the familyOutcomes
	 */
	public Set<FamilyOutcome> getFamilyOutcomes() {
		return familyOutcomes;
	}

	/**
	 * @param familyOutcomes
	 *            the familyOutcomes to set
	 */
	public void setFamilyOutcomes(Set<FamilyOutcome> familyOutcomes) {
		this.familyOutcomes = familyOutcomes;
	}

	/**
	 * @return the financialInspectingNotes
	 */
	public String getFinancialInspectingNotes() {
		return financialInspectingNotes;
	}

	/**
	 * @param financialInspectingNotes
	 *            the financialInspectingNotes to set
	 */
	public void setFinancialInspectingNotes(String financialInspectingNotes) {
		this.financialInspectingNotes = financialInspectingNotes;
	}

	/**
	 * @return the socialSectionNotes
	 */
	public String getSocialSectionNotes() {
		return socialSectionNotes;
	}

	/**
	 * @param socialSectionNotes
	 *            the socialSectionNotes to set
	 */
	public void setSocialSectionNotes(String socialSectionNotes) {
		this.socialSectionNotes = socialSectionNotes;
	}

	/**
	 * @return the aidFinancial
	 */
	public boolean isAidFinancial() {
		return aidFinancial;
	}

	/**
	 * @param aidFinancial
	 *            the aidFinancial to set
	 */
	public void setAidFinancial(boolean aidFinancial) {
		this.aidFinancial = aidFinancial;
	}

	/**
	 * @return the aidFinancialValue
	 */
	public double getAidFinancialValue() {
		return aidFinancialValue;
	}

	/**
	 * @param aidFinancialValue
	 *            the aidFinancialValue to set
	 */
	public void setAidFinancialValue(double aidFinancialValue) {
		this.aidFinancialValue = aidFinancialValue;
	}

	/**
	 * @return the aidFood
	 */
	public boolean isAidFood() {
		return aidFood;
	}

	/**
	 * @param aidFood
	 *            the aidFood to set
	 */
	public void setAidFood(boolean aidFood) {
		this.aidFood = aidFood;
	}

	/**
	 * @return the aidFoodNotes
	 */
	public String getAidFoodNotes() {
		return aidFoodNotes;
	}

	/**
	 * @param aidFoodNotes
	 *            the aidFoodNotes to set
	 */
	public void setAidFoodNotes(String aidFoodNotes) {
		this.aidFoodNotes = aidFoodNotes;
	}

	/**
	 * @return the aidHealth
	 */
	public boolean isAidHealth() {
		return aidHealth;
	}

	/**
	 * @param aidHealth
	 *            the aidHealth to set
	 */
	public void setAidHealth(boolean aidHealth) {
		this.aidHealth = aidHealth;
	}

	/**
	 * @return the aidHealthNotes
	 */
	public String getAidHealthNotes() {
		return aidHealthNotes;
	}

	/**
	 * @param aidHealthNotes
	 *            the aidHealthNotes to set
	 */
	public void setAidHealthNotes(String aidHealthNotes) {
		this.aidHealthNotes = aidHealthNotes;
	}

	/**
	 * @return the aidProjectOwning
	 */
	public boolean isAidProjectOwning() {
		return aidProjectOwning;
	}

	/**
	 * @param aidProjectOwning
	 *            the aidProjectOwning to set
	 */
	public void setAidProjectOwning(boolean aidProjectOwning) {
		this.aidProjectOwning = aidProjectOwning;
	}

	/**
	 * @return the aidProjectType
	 */
	public String getAidProjectType() {
		return aidProjectType;
	}

	/**
	 * @param aidProjectType
	 *            the aidProjectType to set
	 */
	public void setAidProjectType(String aidProjectType) {
		this.aidProjectType = aidProjectType;
	}

	/**
	 * @return the aidProjectValue
	 */
	public double getAidProjectValue() {
		return aidProjectValue;
	}

	/**
	 * @param aidProjectValue
	 *            the aidProjectValue to set
	 */
	public void setAidProjectValue(double aidProjectValue) {
		this.aidProjectValue = aidProjectValue;
	}

	/**
	 * @return the attachments
	 */
	public Set<Attachment> getAttachments() {
		return attachments;
	}

	/**
	 * @param attachments
	 *            the attachments to set
	 */
	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}

	/**
	 * @return the husbandEducationalCertificate
	 */
	public String getHusbandEducationalCertificate() {
		return husbandEducationalCertificate;
	}

	/**
	 * @return the aidFinancialWay
	 */
	public FinancialAidWay getAidFinancialWay() {
		return aidFinancialWay;
	}

	/**
	 * @param aidFinancialWay
	 *            the aidFinancialWay to set
	 */
	public void setAidFinancialWay(FinancialAidWay aidFinancialWay) {
		this.aidFinancialWay = aidFinancialWay;
	}

	/**
	 * @return the aidFinancialType
	 */
	public FinancialAidType getAidFinancialType() {
		return aidFinancialType;
	}

	/**
	 * @param aidFinancialType
	 *            the aidFinancialType to set
	 */
	public void setAidFinancialType(FinancialAidType aidFinancialType) {
		this.aidFinancialType = aidFinancialType;
	}

	/**
	 * @return the aidHealthType
	 */
	public HealthAidType getAidHealthType() {
		return aidHealthType;
	}

	/**
	 * @param aidHealthType
	 *            the aidHealthType to set
	 */
	public void setAidHealthType(HealthAidType aidHealthType) {
		this.aidHealthType = aidHealthType;
	}

	/**
	 * @return the relativefamilyPeoples
	 */
	public Set<RelativeFamilyPeople> getRelativefamilyPeoples() {
		return relativefamilyPeoples;
	}

	/**
	 * @param relativefamilyPeoples
	 *            the relativefamilyPeoples to set
	 */
	public void setRelativefamilyPeoples(Set<RelativeFamilyPeople> relativefamilyPeoples) {
		this.relativefamilyPeoples = relativefamilyPeoples;
	}

	/**
	 * @return the otherFamilyPeoples
	 */
	public Set<OtherFamilyPeople> getOtherFamilyPeoples() {
		return otherFamilyPeoples;
	}

	/**
	 * @param otherFamilyPeoples
	 *            the otherFamilyPeoples to set
	 */
	public void setOtherFamilyPeoples(Set<OtherFamilyPeople> otherFamilyPeoples) {
		this.otherFamilyPeoples = otherFamilyPeoples;
	}

}