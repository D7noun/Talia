package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class RelativeFamilyPeople implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	private String name;
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	private String relationType;

	private boolean currentlyWorking;
	private boolean currentlyStuding;
	private boolean currentlySick;
	private boolean helpsInExpenses;

	private String notes;

	@ManyToOne
	private File file;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the relationType
	 */
	public String getRelationType() {
		return relationType;
	}

	/**
	 * @param relationType
	 *            the relationType to set
	 */
	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	/**
	 * @return the currentlyWorking
	 */
	public boolean isCurrentlyWorking() {
		return currentlyWorking;
	}

	/**
	 * @param currentlyWorking
	 *            the currentlyWorking to set
	 */
	public void setCurrentlyWorking(boolean currentlyWorking) {
		this.currentlyWorking = currentlyWorking;
	}

	/**
	 * @return the currentlyStuding
	 */
	public boolean isCurrentlyStuding() {
		return currentlyStuding;
	}

	/**
	 * @param currentlyStuding
	 *            the currentlyStuding to set
	 */
	public void setCurrentlyStuding(boolean currentlyStuding) {
		this.currentlyStuding = currentlyStuding;
	}

	/**
	 * @return the currentlySick
	 */
	public boolean isCurrentlySick() {
		return currentlySick;
	}

	/**
	 * @param currentlySick
	 *            the currentlySick to set
	 */
	public void setCurrentlySick(boolean currentlySick) {
		this.currentlySick = currentlySick;
	}

	/**
	 * @return the helpsInExpenses
	 */
	public boolean isHelpsInExpenses() {
		return helpsInExpenses;
	}

	/**
	 * @param helpsInExpenses
	 *            the helpsInExpenses to set
	 */
	public void setHelpsInExpenses(boolean helpsInExpenses) {
		this.helpsInExpenses = helpsInExpenses;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (currentlySick ? 1231 : 1237);
		result = prime * result + (currentlyStuding ? 1231 : 1237);
		result = prime * result + (currentlyWorking ? 1231 : 1237);
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + (helpsInExpenses ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result + ((relationType == null) ? 0 : relationType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelativeFamilyPeople other = (RelativeFamilyPeople) obj;
		if (currentlySick != other.currentlySick)
			return false;
		if (currentlyStuding != other.currentlyStuding)
			return false;
		if (currentlyWorking != other.currentlyWorking)
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (helpsInExpenses != other.helpsInExpenses)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (relationType == null) {
			if (other.relationType != null)
				return false;
		} else if (!relationType.equals(other.relationType))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RelativeFamilyPeople [name=" + name + ", dateOfBirth=" + dateOfBirth + ", relationType=" + relationType
				+ ", currentlyWorking=" + currentlyWorking + ", currentlyStuding=" + currentlyStuding
				+ ", currentlySick=" + currentlySick + ", helpsInExpenses=" + helpsInExpenses + ", notes=" + notes
				+ "]";
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
