package org.D7noun.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.D7noun.model.File.AttachmentType;

@NamedQueries({
		@NamedQuery(name = Attachment.getAttacmentsById, query = "SELECT attach FROM Attachment attach where attach.file.id = ?1 ORDER BY attach.size asc"),
		@NamedQuery(name = Attachment.findById, query = "SELECT attach FROM Attachment attach WHERE attach.id = ?1") })
@Entity
public class Attachment implements Serializable {

	public static final String getAttacmentsById = "Attachment.getAttacmentsById";
	public static final String findById = "Attachment.findById";

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	private String fileName;
	private String contentType;
	@Enumerated(EnumType.STRING)
	private AttachmentType attachmentType;
	private long size;
	private String notes;

	@Lob
	private byte[] data;

	/////////////
	@ManyToOne
	private File file;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the size
	 */
	public long getSize() {
		return size;
	}

	public String getSizeInMegaByte() {
		long fraction = 1024;
		double sizeDouble = size;
		if (size != 0) {
			double sizeFraction = sizeDouble / fraction;
			sizeFraction = Math.floor(sizeFraction * 100) / 100;
			return sizeFraction + " KB";
		}
		return "0 MB";
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(long size) {
		this.size = size;
	}

	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the attachmentType
	 */
	public AttachmentType getAttachmentType() {
		return attachmentType;
	}

	/**
	 * @param attachmentType
	 *            the attachmentType to set
	 */
	public void setAttachmentType(AttachmentType attachmentType) {
		this.attachmentType = attachmentType;
	}

}
