package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.D7noun.model.File.SocialStatus;

@Entity
public class WorkingPeople implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	private String name;
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;

	private boolean livingWithFamily;
	private boolean continueStuding;
	private boolean expatriate;

	@Enumerated(EnumType.STRING)
	private SocialStatus socialStatus;

	private double salary;
	private String workType;

	private double contributionPrice;

	@ManyToOne
	private File file;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the livingWithFamily
	 */
	public boolean isLivingWithFamily() {
		return livingWithFamily;
	}

	/**
	 * @param livingWithFamily
	 *            the livingWithFamily to set
	 */
	public void setLivingWithFamily(boolean livingWithFamily) {
		this.livingWithFamily = livingWithFamily;
	}

	/**
	 * @return the continueStuding
	 */
	public boolean isContinueStuding() {
		return continueStuding;
	}

	/**
	 * @param continueStuding
	 *            the continueStuding to set
	 */
	public void setContinueStuding(boolean continueStuding) {
		this.continueStuding = continueStuding;
	}

	/**
	 * @return the expatriate
	 */
	public boolean isExpatriate() {
		return expatriate;
	}

	/**
	 * @param expatriate
	 *            the expatriate to set
	 */
	public void setExpatriate(boolean expatriate) {
		this.expatriate = expatriate;
	}

	/**
	 * @return the socialStatus
	 */
	public SocialStatus getSocialStatus() {
		return socialStatus;
	}

	/**
	 * @param socialStatus
	 *            the socialStatus to set
	 */
	public void setSocialStatus(SocialStatus socialStatus) {
		this.socialStatus = socialStatus;
	}

	/**
	 * @return the salary
	 */
	public double getSalary() {
		return salary;
	}

	/**
	 * @param salary
	 *            the salary to set
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}

	/**
	 * @return the workType
	 */
	public String getWorkType() {
		return workType;
	}

	/**
	 * @param workType
	 *            the workType to set
	 */
	public void setWorkType(String workType) {
		this.workType = workType;
	}

	/**
	 * @return the contributionPrice
	 */
	public double getContributionPrice() {
		return contributionPrice;
	}

	/**
	 * @param contributionPrice
	 *            the contributionPrice to set
	 */
	public void setContributionPrice(double contributionPrice) {
		this.contributionPrice = contributionPrice;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (continueStuding ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(contributionPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + (expatriate ? 1231 : 1237);
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (livingWithFamily ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		temp = Double.doubleToLongBits(salary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((socialStatus == null) ? 0 : socialStatus.hashCode());
		result = prime * result + ((workType == null) ? 0 : workType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkingPeople other = (WorkingPeople) obj;
		if (continueStuding != other.continueStuding)
			return false;
		if (Double.doubleToLongBits(contributionPrice) != Double.doubleToLongBits(other.contributionPrice))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (expatriate != other.expatriate)
			return false;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (livingWithFamily != other.livingWithFamily)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(salary) != Double.doubleToLongBits(other.salary))
			return false;
		if (socialStatus != other.socialStatus)
			return false;
		if (workType == null) {
			if (other.workType != null)
				return false;
		} else if (!workType.equals(other.workType))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WorkingPeople [id=" + id + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", livingWithFamily="
				+ livingWithFamily + ", continueStuding=" + continueStuding + ", expatriate=" + expatriate
				+ ", socialStatus=" + socialStatus + ", salary=" + salary + ", workType=" + workType
				+ ", contributionPrice=" + contributionPrice + ", file=" + file + "]";
	}

}