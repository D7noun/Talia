package org.D7noun.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.facade.AttachmentFacade;
import org.D7noun.model.Attachment;
import org.D7noun.model.Belonging;
import org.D7noun.model.EducationalPeople;
import org.D7noun.model.FamilyAid;
import org.D7noun.model.FamilyIncome;
import org.D7noun.model.FamilyOutcome;
import org.D7noun.model.File;
import org.D7noun.model.Furniture;
import org.D7noun.model.HusbandWork;
import org.D7noun.model.MarriedChildren;
import org.D7noun.model.OtherFamilyPeople;
import org.D7noun.model.RelativeFamilyPeople;
import org.D7noun.model.SickPeople;
import org.D7noun.model.WifeWork;
import org.D7noun.model.WorkingPeople;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 * Backing bean for File entities.
 * <p/>
 * This class provides CRUD functionality for all File entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class FileBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public String getText(String key) {
		return CommonUtility.getTextFromBundle(key);
	}

	/*
	 * Support creating and retrieving File entities
	 */

	private Long id;
	private File file;

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "talia-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
		this.conversation.begin();
		this.conversation.setTimeout(1800000L);
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.file = this.example;

			Furniture f1 = new Furniture(CommonUtility.getTextFromBundle("LivingRoom"));
			f1.setFile(file);
			Furniture f2 = new Furniture(CommonUtility.getTextFromBundle("BedRoom"));
			f2.setFile(file);
			Furniture f3 = new Furniture(CommonUtility.getTextFromBundle("Oven"));
			f3.setFile(file);
			Furniture f4 = new Furniture(CommonUtility.getTextFromBundle("WashingMachine"));
			f4.setFile(file);
			Furniture f5 = new Furniture(CommonUtility.getTextFromBundle("Refrigirator"));
			f5.setFile(file);
			Furniture f6 = new Furniture(CommonUtility.getTextFromBundle("Television"));
			f6.setFile(file);
			Furniture f7 = new Furniture(CommonUtility.getTextFromBundle("Carpet"));
			f7.setFile(file);
			Furniture f8 = new Furniture(CommonUtility.getTextFromBundle("Telephone"));
			f8.setFile(file);
			this.file.getFurnitures().add(f1);
			this.file.getFurnitures().add(f2);
			this.file.getFurnitures().add(f3);
			this.file.getFurnitures().add(f4);
			this.file.getFurnitures().add(f5);
			this.file.getFurnitures().add(f6);
			this.file.getFurnitures().add(f7);
			this.file.getFurnitures().add(f8);

			/////////////////////////////////////
			/////////////////////////////////////
			/////////////////////////////////////
			/////////////////////////////////////
			FamilyIncome familyIncome1 = new FamilyIncome(CommonUtility.getTextFromBundle("FamilyIncomeHusbandWork"));
			familyIncome1.setFile(file);
			FamilyIncome familyIncome2 = new FamilyIncome(CommonUtility.getTextFromBundle("FamilyIncomeWifeWork"));
			familyIncome2.setFile(file);
			FamilyIncome familyIncome3 = new FamilyIncome(CommonUtility.getTextFromBundle("FamilyIncomeChildrenWork"));
			familyIncome3.setFile(file);
			FamilyIncome familyIncome4 = new FamilyIncome(CommonUtility.getTextFromBundle("FamilyIncomeInstitution"));
			familyIncome4.setFile(file);
			FamilyIncome familyIncome5 = new FamilyIncome(CommonUtility.getTextFromBundle("FamilyIncomeParent"));
			familyIncome5.setFile(file);
			FamilyIncome familyIncome6 = new FamilyIncome(CommonUtility.getTextFromBundle("FamilyIncomeRelative"));
			familyIncome6.setFile(file);

			this.file.getFamilyIncomes().add(familyIncome1);
			this.file.getFamilyIncomes().add(familyIncome2);
			this.file.getFamilyIncomes().add(familyIncome3);
			this.file.getFamilyIncomes().add(familyIncome4);
			this.file.getFamilyIncomes().add(familyIncome5);
			this.file.getFamilyIncomes().add(familyIncome6);

			/////////////////////////////////////
			/////////////////////////////////////
			/////////////////////////////////////
			/////////////////////////////////////
			FamilyOutcome familyOutcome1 = new FamilyOutcome(CommonUtility.getTextFromBundle("FamilyOutcomeHouseRent"));
			FamilyOutcome familyOutcome2 = new FamilyOutcome(CommonUtility.getTextFromBundle("FamilyOutcomeShopRent"));
			FamilyOutcome familyOutcome3 = new FamilyOutcome(CommonUtility.getTextFromBundle("FamilyOutcomeFood"));
			FamilyOutcome familyOutcome4 = new FamilyOutcome(CommonUtility.getTextFromBundle("FamilyOutcomeMedicine"));
			FamilyOutcome familyOutcome5 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomeSchoolPayment"));
			FamilyOutcome familyOutcome6 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomeSchoolTransportation"));
			FamilyOutcome familyOutcome7 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomeElectricityDawla"));
			FamilyOutcome familyOutcome8 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomeElectricityEshtirak"));
			FamilyOutcome familyOutcome9 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomePhoneMobile"));
			FamilyOutcome familyOutcome10 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomePhoneFixed"));
			FamilyOutcome familyOutcome11 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomeWaterDawla"));
			FamilyOutcome familyOutcome12 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomeWaterEshtirak"));
			FamilyOutcome familyOutcome13 = new FamilyOutcome(
					CommonUtility.getTextFromBundle("FamilyOutcomeSatellite"));
			FamilyOutcome familyOutcome14 = new FamilyOutcome(CommonUtility.getTextFromBundle("FamilyOutcomeDebts"));
			FamilyOutcome familyOutcome15 = new FamilyOutcome(CommonUtility.getTextFromBundle("FamilyOutcomeLoans"));
			FamilyOutcome familyOutcome16 = new FamilyOutcome(CommonUtility.getTextFromBundle("FamilyOutcomeOthers"));

			familyOutcome1.setFile(file);
			familyOutcome2.setFile(file);
			familyOutcome3.setFile(file);
			familyOutcome4.setFile(file);
			familyOutcome5.setFile(file);
			familyOutcome6.setFile(file);
			familyOutcome7.setFile(file);
			familyOutcome8.setFile(file);
			familyOutcome9.setFile(file);
			familyOutcome10.setFile(file);
			familyOutcome11.setFile(file);
			familyOutcome12.setFile(file);
			familyOutcome13.setFile(file);
			familyOutcome14.setFile(file);
			familyOutcome15.setFile(file);
			familyOutcome16.setFile(file);

			this.file.getFamilyOutcomes().add(familyOutcome1);
			this.file.getFamilyOutcomes().add(familyOutcome2);
			this.file.getFamilyOutcomes().add(familyOutcome3);
			this.file.getFamilyOutcomes().add(familyOutcome4);
			this.file.getFamilyOutcomes().add(familyOutcome5);
			this.file.getFamilyOutcomes().add(familyOutcome6);
			this.file.getFamilyOutcomes().add(familyOutcome7);
			this.file.getFamilyOutcomes().add(familyOutcome8);
			this.file.getFamilyOutcomes().add(familyOutcome9);
			this.file.getFamilyOutcomes().add(familyOutcome10);
			this.file.getFamilyOutcomes().add(familyOutcome11);
			this.file.getFamilyOutcomes().add(familyOutcome12);
			this.file.getFamilyOutcomes().add(familyOutcome13);
			this.file.getFamilyOutcomes().add(familyOutcome14);
			this.file.getFamilyOutcomes().add(familyOutcome15);
			this.file.getFamilyOutcomes().add(familyOutcome16);

		} else {
			this.file = findById(getId());
		}
	}

	public File findById(Long id) {
		return this.entityManager.find(File.class, id);
	}

	/*
	 * Support updating and deleting File entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.file);
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.file);
				return "create?faces-redirect=true&id=" + this.file.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			File deletableEntity = findById(getId());
			Iterator<HusbandWork> iterHusbandWorks = deletableEntity.getHusbandWorks().iterator();
			for (; iterHusbandWorks.hasNext();) {
				HusbandWork nextInHusbandWorks = iterHusbandWorks.next();
				nextInHusbandWorks.setFile(null);
				iterHusbandWorks.remove();
				this.entityManager.merge(nextInHusbandWorks);
			}
			Iterator<WifeWork> iterWifeWorks = deletableEntity.getWifeWorks().iterator();
			for (; iterWifeWorks.hasNext();) {
				WifeWork nextInWifeWorks = iterWifeWorks.next();
				nextInWifeWorks.setFile(null);
				iterWifeWorks.remove();
				this.entityManager.merge(nextInWifeWorks);
			}
			Iterator<RelativeFamilyPeople> iterRelativefamilyPeoples = deletableEntity.getRelativefamilyPeoples()
					.iterator();
			for (; iterRelativefamilyPeoples.hasNext();) {
				RelativeFamilyPeople nextInRelativefamilyPeoples = iterRelativefamilyPeoples.next();
				nextInRelativefamilyPeoples.setFile(null);
				iterRelativefamilyPeoples.remove();
				this.entityManager.merge(nextInRelativefamilyPeoples);
			}
			Iterator<OtherFamilyPeople> iterOtherFamilyPeoples = deletableEntity.getOtherFamilyPeoples().iterator();
			for (; iterOtherFamilyPeoples.hasNext();) {
				OtherFamilyPeople nextInOtherFamilyPeoples = iterOtherFamilyPeoples.next();
				nextInOtherFamilyPeoples.setFile(null);
				iterOtherFamilyPeoples.remove();
				this.entityManager.merge(nextInOtherFamilyPeoples);
			}
			Iterator<MarriedChildren> iterMarriedChildrens = deletableEntity.getMarriedChildrens().iterator();
			for (; iterMarriedChildrens.hasNext();) {
				MarriedChildren nextInMarriedChildrens = iterMarriedChildrens.next();
				nextInMarriedChildrens.setFile(null);
				iterMarriedChildrens.remove();
				this.entityManager.merge(nextInMarriedChildrens);
			}
			Iterator<EducationalPeople> iterEducationalPeoples = deletableEntity.getEducationalPeoples().iterator();
			for (; iterEducationalPeoples.hasNext();) {
				EducationalPeople nextInEducationalPeoples = iterEducationalPeoples.next();
				nextInEducationalPeoples.setFile(null);
				iterEducationalPeoples.remove();
				this.entityManager.merge(nextInEducationalPeoples);
			}
			Iterator<SickPeople> iterSickPeoples = deletableEntity.getSickPeoples().iterator();
			for (; iterSickPeoples.hasNext();) {
				SickPeople nextInSickPeoples = iterSickPeoples.next();
				nextInSickPeoples.setFile(null);
				iterSickPeoples.remove();
				this.entityManager.merge(nextInSickPeoples);
			}
			Iterator<WorkingPeople> iterWorkingPeoples = deletableEntity.getWorkingPeoples().iterator();
			for (; iterWorkingPeoples.hasNext();) {
				WorkingPeople nextInWorkingPeoples = iterWorkingPeoples.next();
				nextInWorkingPeoples.setFile(null);
				iterWorkingPeoples.remove();
				this.entityManager.merge(nextInWorkingPeoples);
			}
			Iterator<Furniture> iterFurnitures = deletableEntity.getFurnitures().iterator();
			for (; iterFurnitures.hasNext();) {
				Furniture nextInFurnitures = iterFurnitures.next();
				nextInFurnitures.setFile(null);
				iterFurnitures.remove();
				this.entityManager.merge(nextInFurnitures);
			}
			Iterator<Belonging> iterBelongings = deletableEntity.getBelongings().iterator();
			for (; iterBelongings.hasNext();) {
				Belonging nextInBelongings = iterBelongings.next();
				nextInBelongings.setFile(null);
				iterBelongings.remove();
				this.entityManager.merge(nextInBelongings);
			}
			Iterator<FamilyAid> iterFamilyAids = deletableEntity.getFamilyAids().iterator();
			for (; iterFamilyAids.hasNext();) {
				FamilyAid nextInFamilyAids = iterFamilyAids.next();
				nextInFamilyAids.setFile(null);
				iterFamilyAids.remove();
				this.entityManager.merge(nextInFamilyAids);
			}
			Iterator<FamilyIncome> iterFamilyIncomes = deletableEntity.getFamilyIncomes().iterator();
			for (; iterFamilyIncomes.hasNext();) {
				FamilyIncome nextInFamilyIncomes = iterFamilyIncomes.next();
				nextInFamilyIncomes.setFile(null);
				iterFamilyIncomes.remove();
				this.entityManager.merge(nextInFamilyIncomes);
			}
			Iterator<FamilyOutcome> iterFamilyOutcomes = deletableEntity.getFamilyOutcomes().iterator();
			for (; iterFamilyOutcomes.hasNext();) {
				FamilyOutcome nextInFamilyOutcomes = iterFamilyOutcomes.next();
				nextInFamilyOutcomes.setFile(null);
				iterFamilyOutcomes.remove();
				this.entityManager.merge(nextInFamilyOutcomes);
			}
			Iterator<Attachment> iterAttachments = deletableEntity.getAttachments().iterator();
			for (; iterAttachments.hasNext();) {
				Attachment nextInAttachments = iterAttachments.next();
				nextInAttachments.setFile(null);
				iterAttachments.remove();
				this.entityManager.merge(nextInAttachments);
			}
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching File entities with pagination
	 */

	private int page;
	private long count;
	private List<File> pageItems;

	private File example = new File();

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<File> root = countCriteria.from(File.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<File> criteria = builder.createQuery(File.class);
		root = criteria.from(File.class);
		TypedQuery<File> query = this.entityManager.createQuery(criteria.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<File> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String husbandName = this.example.getHusbandName();
		if (husbandName != null && !"".equals(husbandName)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("husbandName")),
					'%' + husbandName.toLowerCase() + '%'));
		}
		String wifeName = this.example.getWifeName();
		if (wifeName != null && !"".equals(wifeName)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("wifeName")), '%' + wifeName.toLowerCase() + '%'));
		}
		String caseType = this.example.getCaseType();
		if (caseType != null && !"".equals(caseType)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("caseType")), '%' + caseType.toLowerCase() + '%'));
		}
		String homeAddress = this.example.getHomeAddress();
		if (homeAddress != null && !"".equals(homeAddress)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("homeAddress")),
					'%' + homeAddress.toLowerCase() + '%'));
		}
		String firstPhoneNumber = this.example.getFirstPhoneNumber();
		if (firstPhoneNumber != null && !"".equals(firstPhoneNumber)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("firstPhoneNumber")),
					'%' + firstPhoneNumber.toLowerCase() + '%'));
		}
		String inspector = this.example.getInspector();
		if (inspector != null && !"".equals(inspector)) {
			predicatesList.add(
					builder.like(builder.lower(root.<String>get("inspector")), '%' + inspector.toLowerCase() + '%'));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<File> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back File entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<File> getAll() {

		CriteriaQuery<File> criteria = this.entityManager.getCriteriaBuilder().createQuery(File.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(File.class))).getResultList();
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private File add = new File();

	public File getAdd() {
		return this.add;
	}

	public File getAdded() {
		File added = this.add;
		this.add = new File();
		return added;
	}

	/**
	 * 
	 * Tables
	 * 
	 */

	private HusbandWork editHusbandWork;
	private HusbandWork deleteHusbandWork;
	private WifeWork editWifeWork;
	private WifeWork deleteWifeWork;
	private RelativeFamilyPeople editRelativeFamilyPeople;
	private RelativeFamilyPeople deleteRelativeFamilyPeople;
	private OtherFamilyPeople editOtherFamilyPeople;
	private OtherFamilyPeople deleteOtherFamilyPeople;
	private MarriedChildren editMarriedChildren;
	private MarriedChildren deleteMarriedChildren;
	private EducationalPeople editEducationalPeople;
	private EducationalPeople deleteEducationalPeople;
	private SickPeople editSickPeople;
	private SickPeople deleteSickPeople;
	private WorkingPeople editWorkingPeople;
	private WorkingPeople deleteWorkingPeople;
	private Furniture editFurniture;
	private Belonging editBelonging;
	private Belonging deleteBelonging;
	private FamilyAid editFamilyAid;
	private FamilyAid deleteFamilyAid;
	private FamilyIncome editFamilyIncome;
	private FamilyOutcome editFamilyOutcome;

	private boolean editHusbandWorkBoolean;
	private boolean editWifeWorkBoolean;
	private boolean editRelativeFamilyBoolean;
	private boolean editOtherFamilyBoolean;
	private boolean editMarriedBoolean;
	private boolean editEducationBoolean;
	private boolean editSickBoolean;
	private boolean editWorkingPeopleBoolean;
	private boolean editBelongingBoolean;
	private boolean editAidBoolean;

	@PostConstruct
	public void init() {
		editHusbandWork = new HusbandWork();
		editWifeWork = new WifeWork();
		editRelativeFamilyPeople = new RelativeFamilyPeople();
		editOtherFamilyPeople = new OtherFamilyPeople();
		editMarriedChildren = new MarriedChildren();
		editEducationalPeople = new EducationalPeople();
		editSickPeople = new SickPeople();
		editWorkingPeople = new WorkingPeople();
		editFurniture = new Furniture();
		editBelonging = new Belonging();
		editFamilyAid = new FamilyAid();
		editFamilyIncome = new FamilyIncome();
		editFamilyOutcome = new FamilyOutcome();
	}

	public void openAddHusbandWorkDialog() {
		editHusbandWork = new HusbandWork();
		editHusbandWorkBoolean = false;

		Ajax.update("editHusbandWorkDialog");
		Ajax.oncomplete("PF('editHusbandWorkDialogVar').show()");
	}

	public void openEditHusbandWorkDialog(HusbandWork husbandWork) {
		editHusbandWork = husbandWork;
		editHusbandWorkBoolean = true;
	}

	public void editHusbandWork() {
		this.file.getHusbandWorks().add(editHusbandWork);
		editHusbandWork.setFile(file);
	}

	public void selectHusbandWorkForDelete(HusbandWork husbandWork) {
		deleteHusbandWork = husbandWork;
	}

	public void removeHusbandWork() {
		this.file.getHusbandWorks().remove(deleteHusbandWork);
	}

	//////////////////////////////////////

	public void openAddWifeWorkDialog() {
		editWifeWork = new WifeWork();
		editWifeWorkBoolean = false;

		Ajax.update("editWifeWorkDialog");
		Ajax.oncomplete("PF('editWifeWorkDialogVar').show()");
	}

	public void openEditWifeWorkDialog(WifeWork wifeWork) {
		editWifeWork = wifeWork;
		editWifeWorkBoolean = true;
	}

	public void editWifeWork() {
		this.file.getWifeWorks().add(editWifeWork);
		editWifeWork.setFile(file);
	}

	public void selectWifeWorkForDelete(WifeWork wifeWork) {
		deleteWifeWork = wifeWork;
	}

	public void removeWifeWork() {
		this.file.getWifeWorks().remove(deleteWifeWork);
	}

	//////////////////////////////////////

	public void openAddRelativeFamilyPeopleDialog() {
		editRelativeFamilyPeople = new RelativeFamilyPeople();
		editRelativeFamilyBoolean = false;

		Ajax.update("editRelativeFamilyPeopleDialog");
		Ajax.oncomplete("PF('editRelativeFamilyPeopleDialogVar').show()");
	}

	public void openEditRelativeFamilyPeopleDialog(RelativeFamilyPeople familyPeople) {
		editRelativeFamilyPeople = familyPeople;
		editRelativeFamilyBoolean = true;
	}

	public void editRelativeFamilyPeople() {
		this.file.getRelativefamilyPeoples().add(editRelativeFamilyPeople);
		editRelativeFamilyPeople.setFile(file);
	}

	public void selectRelativeFamilyPeopleForDelete(RelativeFamilyPeople familyPeople) {
		deleteRelativeFamilyPeople = familyPeople;
	}

	public void removeRelativeFamilyPeople() {
		this.file.getRelativefamilyPeoples().remove(deleteRelativeFamilyPeople);
	}

	//////////////////////////////////////

	public void openAddOtherFamilyPeopleDialog() {
		editOtherFamilyPeople = new OtherFamilyPeople();
		editOtherFamilyBoolean = false;

		Ajax.update("editOtherFamilyPeopleDialog");
		Ajax.oncomplete("PF('editOtherFamilyPeopleDialogVar').show()");
	}

	public void openEditOtherFamilyPeopleDialog(OtherFamilyPeople familyPeople) {
		editOtherFamilyPeople = familyPeople;
		editOtherFamilyBoolean = true;
	}

	public void editOtherFamilyPeople() {
		this.file.getOtherFamilyPeoples().add(editOtherFamilyPeople);
		editOtherFamilyPeople.setFile(file);
	}

	public void selectOtherFamilyPeopleForDelete(OtherFamilyPeople familyPeople) {
		deleteOtherFamilyPeople = familyPeople;
	}

	public void removeOtherFamilyPeople() {
		this.file.getOtherFamilyPeoples().remove(deleteOtherFamilyPeople);
	}

	//////////////////////////////////////

	public void openAddMarriedChildrenDialog() {
		editMarriedChildren = new MarriedChildren();
		editMarriedBoolean = false;

		Ajax.update("editMarriedChildrenDialog");
		Ajax.oncomplete("PF('editMarriedChildrenDialogVar').show()");
	}

	public void openEditMarriedChildrenDialog(MarriedChildren marriedChildren) {
		editMarriedChildren = marriedChildren;
		editMarriedBoolean = true;
	}

	public void editMarriedChildren() {
		this.file.getMarriedChildrens().add(editMarriedChildren);
		editMarriedChildren.setFile(file);
	}

	public void selectMarriedChildrenForDelete(MarriedChildren marriedChildren) {
		deleteMarriedChildren = marriedChildren;
	}

	public void removeMarriedChildren() {
		this.file.getMarriedChildrens().remove(deleteMarriedChildren);
	}

	//////////////////////////////////////

	public void openAddEducationalPeopleDialog() {
		editEducationalPeople = new EducationalPeople();
		editEducationBoolean = false;

		Ajax.update("editEducationalPeopleDialog");
		Ajax.oncomplete("PF('editEducationalPeopleDialogVar').show()");
	}

	public void openEditEducationalPeopleDialog(EducationalPeople educationalPeople) {
		editEducationalPeople = educationalPeople;
		editEducationBoolean = true;
	}

	public void editEducationalPeople() {
		this.file.getEducationalPeoples().add(editEducationalPeople);
		editEducationalPeople.setFile(file);
	}

	public void selectEducationalPeopleForDelete(EducationalPeople educationalPeople) {
		deleteEducationalPeople = educationalPeople;
	}

	public void removeEducationalPeople() {
		this.file.getEducationalPeoples().remove(deleteEducationalPeople);
	}

	//////////////////////////////////////

	public void openAddSickPeopleDialog() {
		editSickPeople = new SickPeople();
		editSickBoolean = false;

		Ajax.update("editSickPeopleDialog");
		Ajax.oncomplete("PF('editSickPeopleDialogVar').show()");
	}

	public void openEditSickPeopleDialog(SickPeople sickPeople) {
		editSickPeople = sickPeople;
		editSickBoolean = true;
	}

	public void editSickPeople() {
		this.file.getSickPeoples().add(editSickPeople);
		editSickPeople.setFile(file);
	}

	public void selectSickPeopleForDelete(SickPeople sickPeople) {
		deleteSickPeople = sickPeople;
	}

	public void removeSickPeople() {
		this.file.getSickPeoples().remove(deleteSickPeople);
	}

	//////////////////////////////////////

	public void openAddWorkingPeopleDialog() {
		editWorkingPeople = new WorkingPeople();
		editWorkingPeopleBoolean = false;

		Ajax.update("editWorkingPeopleDialog");
		Ajax.oncomplete("PF('editWorkingPeopleDialogVar').show()");
	}

	public void openEditWorkingPeopleDialog(WorkingPeople workingPeople) {
		editWorkingPeople = workingPeople;
		editWorkingPeopleBoolean = true;
	}

	public void editWorkingPeople() {
		this.file.getWorkingPeoples().add(editWorkingPeople);
		editWorkingPeople.setFile(file);
	}

	public void selectWorkingPeopleForDelete(WorkingPeople workingPeople) {
		deleteWorkingPeople = workingPeople;
	}

	public void removeWorkingPeople() {
		this.file.getWorkingPeoples().remove(deleteWorkingPeople);
	}

	//////////////////////////////////////

	public void openEditFurnitureDialog(Furniture furniture) {
		editFurniture = furniture;
	}

	//////////////////////////////////////

	public void openAddBelongingDialog() {
		editBelonging = new Belonging();
		editBelongingBoolean = false;

		Ajax.update("editBelongingDialog");
		Ajax.oncomplete("PF('editBelongingDialogVar').show()");
	}

	public void openEditBelongingDialog(Belonging belonging) {
		editBelonging = belonging;
		editBelongingBoolean = true;
	}

	public void editBelonging() {
		this.file.getBelongings().add(editBelonging);
		editBelonging.setFile(file);
	}

	public void selectBelongingForDelete(Belonging belonging) {
		deleteBelonging = belonging;
	}

	public void removeBelonging() {
		this.file.getBelongings().remove(deleteBelonging);
	}

	//////////////////////////////////////

	public void openAddFamilyAidDialog() {
		editFamilyAid = new FamilyAid();
		editAidBoolean = false;

		Ajax.update("editFamilyAidDialog");
		Ajax.oncomplete("PF('editFamilyAidDialogVar').show()");
	}

	public void openEditFamilyAidDialog(FamilyAid familyAid) {
		editFamilyAid = familyAid;
		editAidBoolean = true;
	}

	public void editFamilyAid() {
		this.file.getFamilyAids().add(editFamilyAid);
		editFamilyAid.setFile(file);
	}

	public void selectFamilyAidForDelete(FamilyAid familyAid) {
		deleteFamilyAid = familyAid;
	}

	public void removeFamilyAid() {
		this.file.getFamilyAids().remove(deleteFamilyAid);
	}

	//////////////////////////////////////

	public void openEditFamilyIncomeDialog(FamilyIncome familyIncome) {
		editFamilyIncome = familyIncome;
	}

	public double incomeSum() {
		try {
			double total = 0;
			for (FamilyIncome familyIncome : this.file.getFamilyIncomes()) {
				total += familyIncome.getValue();
			}
			return total;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: incomeSum");
		}
		return 0;

	}

	//////////////////////////////////////

	public void openEditFamilyOutcomeDialog(FamilyOutcome familyOutcome) {
		editFamilyOutcome = familyOutcome;
	}

	public double outcomeSum() {
		try {
			double total = 0;
			for (FamilyOutcome familyOutcome : this.file.getFamilyOutcomes()) {
				total += familyOutcome.getValue();
			}
			return total;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: outcomeSum");
		}
		return 0;

	}

	/**
	 * 
	 * Start Attachments
	 * 
	 */

	public void handleFileUpload(FileUploadEvent event) {
		try {

			UploadedFile uploadedFile = event.getFile();

			Attachment attachment = new Attachment();
			attachment.setFile(file);
			attachment.setFileName(uploadedFile.getFileName());
			attachment.setSize(uploadedFile.getSize());
			attachment.setContentType(uploadedFile.getContentType());
			attachment.setData(uploadedFile.getContents());
			file.getAttachments().add(attachment);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("ItemBean: handleFileUpload");
		}
	}

	public void download(Attachment attachment) {
		try {
			Faces.sendFile(attachment.getData(), attachment.getFileName(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@EJB
	private AttachmentFacade attachmentFacade;
	private Attachment attachmentFordelete;

	public void selectForDelete(Attachment attachment) {
		attachmentFordelete = attachment;
	}

	public void deleteAttachment() {
		try {
			file.getAttachments().remove(attachmentFordelete);
			attachmentFacade.removeAttachmentFromTable(file, attachmentFordelete);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public StreamedContent getPicture() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else {
			String attachmentId = context.getExternalContext().getRequestParameterMap().get("attachmentId");
			if (attachmentId != null) {
				Attachment attachmentItem = attachmentFacade.findById(Long.parseLong(attachmentId));
				StreamedContent streamedContent = new DefaultStreamedContent(
						new ByteArrayInputStream(attachmentItem.getData()));
				return streamedContent;
			}
		}
		return new DefaultStreamedContent();
	}

	/**
	 * 
	 * End Attachments
	 * 
	 */

	/*********************************
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * ******************************/

	/**
	 * @return the editHusbandWork
	 */
	public HusbandWork getEditHusbandWork() {
		return editHusbandWork;
	}

	/**
	 * @param editHusbandWork
	 *            the editHusbandWork to set
	 */
	public void setEditHusbandWork(HusbandWork editHusbandWork) {
		this.editHusbandWork = editHusbandWork;
	}

	/**
	 * @return the deleteHusbandWork
	 */
	public HusbandWork getDeleteHusbandWork() {
		return deleteHusbandWork;
	}

	/**
	 * @param deleteHusbandWork
	 *            the deleteHusbandWork to set
	 */
	public void setDeleteHusbandWork(HusbandWork deleteHusbandWork) {
		this.deleteHusbandWork = deleteHusbandWork;
	}

	/**
	 * @return the editWifeWork
	 */
	public WifeWork getEditWifeWork() {
		return editWifeWork;
	}

	/**
	 * @param editWifeWork
	 *            the editWifeWork to set
	 */
	public void setEditWifeWork(WifeWork editWifeWork) {
		this.editWifeWork = editWifeWork;
	}

	/**
	 * @return the deleteWifeWork
	 */
	public WifeWork getDeleteWifeWork() {
		return deleteWifeWork;
	}

	/**
	 * @param deleteWifeWork
	 *            the deleteWifeWork to set
	 */
	public void setDeleteWifeWork(WifeWork deleteWifeWork) {
		this.deleteWifeWork = deleteWifeWork;
	}

	/**
	 * @return the editRelativeFamilyPeople
	 */
	public RelativeFamilyPeople getEditRelativeFamilyPeople() {
		return editRelativeFamilyPeople;
	}

	/**
	 * @param editRelativeFamilyPeople
	 *            the editRelativeFamilyPeople to set
	 */
	public void setEditRelativeFamilyPeople(RelativeFamilyPeople editRelativeFamilyPeople) {
		this.editRelativeFamilyPeople = editRelativeFamilyPeople;
	}

	/**
	 * @return the deleteRelativeFamilyPeople
	 */
	public RelativeFamilyPeople getDeleteRelativeFamilyPeople() {
		return deleteRelativeFamilyPeople;
	}

	/**
	 * @param deleteRelativeFamilyPeople
	 *            the deleteRelativeFamilyPeople to set
	 */
	public void setDeleteRelativeFamilyPeople(RelativeFamilyPeople deleteRelativeFamilyPeople) {
		this.deleteRelativeFamilyPeople = deleteRelativeFamilyPeople;
	}

	/**
	 * @return the editOtherFamilyPeople
	 */
	public OtherFamilyPeople getEditOtherFamilyPeople() {
		return editOtherFamilyPeople;
	}

	/**
	 * @param editOtherFamilyPeople
	 *            the editOtherFamilyPeople to set
	 */
	public void setEditOtherFamilyPeople(OtherFamilyPeople editOtherFamilyPeople) {
		this.editOtherFamilyPeople = editOtherFamilyPeople;
	}

	/**
	 * @return the deleteOtherFamilyPeople
	 */
	public OtherFamilyPeople getDeleteOtherFamilyPeople() {
		return deleteOtherFamilyPeople;
	}

	/**
	 * @param deleteOtherFamilyPeople
	 *            the deleteOtherFamilyPeople to set
	 */
	public void setDeleteOtherFamilyPeople(OtherFamilyPeople deleteOtherFamilyPeople) {
		this.deleteOtherFamilyPeople = deleteOtherFamilyPeople;
	}

	/**
	 * @return the editMarriedChildren
	 */
	public MarriedChildren getEditMarriedChildren() {
		return editMarriedChildren;
	}

	/**
	 * @param editMarriedChildren
	 *            the editMarriedChildren to set
	 */
	public void setEditMarriedChildren(MarriedChildren editMarriedChildren) {
		this.editMarriedChildren = editMarriedChildren;
	}

	/**
	 * @return the deleteMarriedChildren
	 */
	public MarriedChildren getDeleteMarriedChildren() {
		return deleteMarriedChildren;
	}

	/**
	 * @param deleteMarriedChildren
	 *            the deleteMarriedChildren to set
	 */
	public void setDeleteMarriedChildren(MarriedChildren deleteMarriedChildren) {
		this.deleteMarriedChildren = deleteMarriedChildren;
	}

	/**
	 * @return the editEducationalPeople
	 */
	public EducationalPeople getEditEducationalPeople() {
		return editEducationalPeople;
	}

	/**
	 * @param editEducationalPeople
	 *            the editEducationalPeople to set
	 */
	public void setEditEducationalPeople(EducationalPeople editEducationalPeople) {
		this.editEducationalPeople = editEducationalPeople;
	}

	/**
	 * @return the deleteEducationalPeople
	 */
	public EducationalPeople getDeleteEducationalPeople() {
		return deleteEducationalPeople;
	}

	/**
	 * @param deleteEducationalPeople
	 *            the deleteEducationalPeople to set
	 */
	public void setDeleteEducationalPeople(EducationalPeople deleteEducationalPeople) {
		this.deleteEducationalPeople = deleteEducationalPeople;
	}

	/**
	 * @return the editSickPeople
	 */
	public SickPeople getEditSickPeople() {
		return editSickPeople;
	}

	/**
	 * @param editSickPeople
	 *            the editSickPeople to set
	 */
	public void setEditSickPeople(SickPeople editSickPeople) {
		this.editSickPeople = editSickPeople;
	}

	/**
	 * @return the deleteSickPeople
	 */
	public SickPeople getDeleteSickPeople() {
		return deleteSickPeople;
	}

	/**
	 * @param deleteSickPeople
	 *            the deleteSickPeople to set
	 */
	public void setDeleteSickPeople(SickPeople deleteSickPeople) {
		this.deleteSickPeople = deleteSickPeople;
	}

	/**
	 * @return the editWorkingPeople
	 */
	public WorkingPeople getEditWorkingPeople() {
		return editWorkingPeople;
	}

	/**
	 * @param editWorkingPeople
	 *            the editWorkingPeople to set
	 */
	public void setEditWorkingPeople(WorkingPeople editWorkingPeople) {
		this.editWorkingPeople = editWorkingPeople;
	}

	/**
	 * @return the deleteWorkingPeople
	 */
	public WorkingPeople getDeleteWorkingPeople() {
		return deleteWorkingPeople;
	}

	/**
	 * @param deleteWorkingPeople
	 *            the deleteWorkingPeople to set
	 */
	public void setDeleteWorkingPeople(WorkingPeople deleteWorkingPeople) {
		this.deleteWorkingPeople = deleteWorkingPeople;
	}

	/**
	 * @return the editFurniture
	 */
	public Furniture getEditFurniture() {
		return editFurniture;
	}

	/**
	 * @param editFurniture
	 *            the editFurniture to set
	 */
	public void setEditFurniture(Furniture editFurniture) {
		this.editFurniture = editFurniture;
	}

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public File getExample() {
		return this.example;
	}

	public void setExample(File example) {
		this.example = example;
	}

	public File getFile() {
		return this.file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the editBelonging
	 */
	public Belonging getEditBelonging() {
		return editBelonging;
	}

	/**
	 * @param editBelonging
	 *            the editBelonging to set
	 */
	public void setEditBelonging(Belonging editBelonging) {
		this.editBelonging = editBelonging;
	}

	/**
	 * @return the deleteBelonging
	 */
	public Belonging getDeleteBelonging() {
		return deleteBelonging;
	}

	/**
	 * @param deleteBelonging
	 *            the deleteBelonging to set
	 */
	public void setDeleteBelonging(Belonging deleteBelonging) {
		this.deleteBelonging = deleteBelonging;
	}

	/**
	 * @return the editFamilyAid
	 */
	public FamilyAid getEditFamilyAid() {
		return editFamilyAid;
	}

	/**
	 * @param editFamilyAid
	 *            the editFamilyAid to set
	 */
	public void setEditFamilyAid(FamilyAid editFamilyAid) {
		this.editFamilyAid = editFamilyAid;
	}

	/**
	 * @return the deleteFamilyAid
	 */
	public FamilyAid getDeleteFamilyAid() {
		return deleteFamilyAid;
	}

	/**
	 * @param deleteFamilyAid
	 *            the deleteFamilyAid to set
	 */
	public void setDeleteFamilyAid(FamilyAid deleteFamilyAid) {
		this.deleteFamilyAid = deleteFamilyAid;
	}

	/**
	 * @return the editFamilyIncome
	 */
	public FamilyIncome getEditFamilyIncome() {
		return editFamilyIncome;
	}

	/**
	 * @param editFamilyIncome
	 *            the editFamilyIncome to set
	 */
	public void setEditFamilyIncome(FamilyIncome editFamilyIncome) {
		this.editFamilyIncome = editFamilyIncome;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the editFamilyOutcome
	 */
	public FamilyOutcome getEditFamilyOutcome() {
		return editFamilyOutcome;
	}

	/**
	 * @param editFamilyOutcome
	 *            the editFamilyOutcome to set
	 */
	public void setEditFamilyOutcome(FamilyOutcome editFamilyOutcome) {
		this.editFamilyOutcome = editFamilyOutcome;
	}

	/**
	 * @return the editHusbandWorkBoolean
	 */
	public boolean isEditHusbandWorkBoolean() {
		return editHusbandWorkBoolean;
	}

	/**
	 * @param editHusbandWorkBoolean
	 *            the editHusbandWorkBoolean to set
	 */
	public void setEditHusbandWorkBoolean(boolean editHusbandWorkBoolean) {
		this.editHusbandWorkBoolean = editHusbandWorkBoolean;
	}

	/**
	 * @return the editWifeWorkBoolean
	 */
	public boolean isEditWifeWorkBoolean() {
		return editWifeWorkBoolean;
	}

	/**
	 * @param editWifeWorkBoolean
	 *            the editWifeWorkBoolean to set
	 */
	public void setEditWifeWorkBoolean(boolean editWifeWorkBoolean) {
		this.editWifeWorkBoolean = editWifeWorkBoolean;
	}

	/**
	 * @return the editRelativeFamilyBoolean
	 */
	public boolean isEditRelativeFamilyBoolean() {
		return editRelativeFamilyBoolean;
	}

	/**
	 * @param editRelativeFamilyBoolean
	 *            the editRelativeFamilyBoolean to set
	 */
	public void setEditRelativeFamilyBoolean(boolean editRelativeFamilyBoolean) {
		this.editRelativeFamilyBoolean = editRelativeFamilyBoolean;
	}

	/**
	 * @return the editOtherFamilyBoolean
	 */
	public boolean isEditOtherFamilyBoolean() {
		return editOtherFamilyBoolean;
	}

	/**
	 * @param editOtherFamilyBoolean
	 *            the editOtherFamilyBoolean to set
	 */
	public void setEditOtherFamilyBoolean(boolean editOtherFamilyBoolean) {
		this.editOtherFamilyBoolean = editOtherFamilyBoolean;
	}

	/**
	 * @return the editMarriedBoolean
	 */
	public boolean isEditMarriedBoolean() {
		return editMarriedBoolean;
	}

	/**
	 * @param editMarriedBoolean
	 *            the editMarriedBoolean to set
	 */
	public void setEditMarriedBoolean(boolean editMarriedBoolean) {
		this.editMarriedBoolean = editMarriedBoolean;
	}

	/**
	 * @return the editEducationBoolean
	 */
	public boolean isEditEducationBoolean() {
		return editEducationBoolean;
	}

	/**
	 * @param editEducationBoolean
	 *            the editEducationBoolean to set
	 */
	public void setEditEducationBoolean(boolean editEducationBoolean) {
		this.editEducationBoolean = editEducationBoolean;
	}

	/**
	 * @return the editSickBoolean
	 */
	public boolean isEditSickBoolean() {
		return editSickBoolean;
	}

	/**
	 * @param editSickBoolean
	 *            the editSickBoolean to set
	 */
	public void setEditSickBoolean(boolean editSickBoolean) {
		this.editSickBoolean = editSickBoolean;
	}

	/**
	 * @return the editWorkingPeopleBoolean
	 */
	public boolean isEditWorkingPeopleBoolean() {
		return editWorkingPeopleBoolean;
	}

	/**
	 * @param editWorkingPeopleBoolean
	 *            the editWorkingPeopleBoolean to set
	 */
	public void setEditWorkingPeopleBoolean(boolean editWorkingPeopleBoolean) {
		this.editWorkingPeopleBoolean = editWorkingPeopleBoolean;
	}

	/**
	 * @return the editBelongingBoolean
	 */
	public boolean isEditBelongingBoolean() {
		return editBelongingBoolean;
	}

	/**
	 * @param editBelongingBoolean
	 *            the editBelongingBoolean to set
	 */
	public void setEditBelongingBoolean(boolean editBelongingBoolean) {
		this.editBelongingBoolean = editBelongingBoolean;
	}

	/**
	 * @return the editAidBoolean
	 */
	public boolean isEditAidBoolean() {
		return editAidBoolean;
	}

	/**
	 * @param editAidBoolean
	 *            the editAidBoolean to set
	 */
	public void setEditAidBoolean(boolean editAidBoolean) {
		this.editAidBoolean = editAidBoolean;
	}

}