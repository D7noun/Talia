package org.D7noun.view;

import java.util.ResourceBundle;

public class CommonUtility {

	public static String getTextFromBundle(String key) {
		try {
			if (key == null || key.equals("")) {
				return "";
			}
			return ResourceBundle.getBundle("resources.application").getString(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}