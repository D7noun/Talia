$(document).ready(function() {
	$('.main-information').show(700);
});

function mainInformation() {
	$('.snippet').hide(700);
	$('.main-information').show(700);
	$('.navigation-button').removeClass('active');
	$('.main-information-button').addClass('active');
}

function husbandPersonalInformation() {
	$('.snippet').hide(700);
	$('.husband-personal-information').show(700);
	$('.navigation-button').removeClass('active');
	$('.husband-personal-button').addClass('active');
}

function husbandJobInformation() {
	$('.snippet').hide(700);
	$('.husband-job-information').show(700);
	$('.navigation-button').removeClass('active');
	$('.husband-work-button').addClass('active');
}

function wifePersonalInformation() {
	$('.snippet').hide(700);
	$('.wife-personal-information').show(700);
	$('.navigation-button').removeClass('active');
	$('.wife-personal-button').addClass('active');
}

function wifeJobInformation() {
	$('.snippet').hide(700);
	$('.wife-job-information').show(700);
	$('.navigation-button').removeClass('active');
	$('.wife-job-button').addClass('active');
}

function relativefamilyPeoples() {
	$('.snippet').hide(700);
	$('.relativefamilyPeoples').show(700);
	$('.navigation-button').removeClass('active');
	$('.relative-people-button').addClass('active');
}

function familyPeoplePrivate() {
	$('.snippet').hide(700);
	$('.familyPeoplePrivate').show(700);
	$('.navigation-button').removeClass('active');
	$('.family-people-private-button').addClass('active');
}

function marriedChildren() {
	$('.snippet').hide(700);
	$('.marriedChildren').show(700);
	$('.navigation-button').removeClass('active');
	$('.married-children-button').addClass('active');
}
function educationalPeople() {
	$('.snippet').hide(700);
	$('.educationalPeople').show(700);
	$('.navigation-button').removeClass('active');
	$('.educational-people-button').addClass('active');
}
function sickPeople() {
	$('.snippet').hide(700);
	$('.sickPeople').show(700);
	$('.navigation-button').removeClass('active');
	$('.sick-people-button').addClass('active');
}

function workingPeopleFamily() {
	$('.snippet').hide(700);
	$('.workingPeople').show(700);
	$('.navigation-button').removeClass('active');
	$('.working-people-button').addClass('active');
}

function residence() {
	$('.snippet').hide(700);
	$('.residence').show(700);
	$('.navigation-button').removeClass('active');
	$('.residence-button').addClass('active');
}

function belongings() {
	$('.snippet').hide(700);
	$('.belongings').show(700);
	$('.navigation-button').removeClass('active');
	$('.belongings-button').addClass('active');
}

function aid() {
	$('.snippet').hide(700);
	$('.aid').show(700);
	$('.navigation-button').removeClass('active');
	$('.aids-button').addClass('active');
}
function income() {
	$('.snippet').hide(700);
	$('.income').show(700);
	$('.navigation-button').removeClass('active');
	$('.incomes-button').addClass('active');
}
function outcome() {
	$('.snippet').hide(700);
	$('.outcome').show(700);
	$('.navigation-button').removeClass('active');
	$('.outcomes-button').addClass('active');
}

function notes() {
	$('.snippet').hide(700);
	$('.notes').show(700);
	$('.navigation-button').removeClass('active');
	$('.section-notes-button').addClass('active');
}

function recommendedAids() {
	$('.snippet').hide(700);
	$('.recommended-aids').show(700);
	$('.navigation-button').removeClass('active');
	$('.aid-types-button').addClass('active');
}

function attachments() {
	$('.snippet').hide(700);
	$('.attachments').show(700);
	$('.navigation-button').removeClass('active');
	$('.attachments-button').addClass('active');
}

/** ************************************************** */
/** ************************************************** */
/** ************************************************** */
/** ************************************************** */
/** ************************************************** */
/** ************************************************** */
/** ************************************************** */
/** ************************************************** */

function updateComponents() {
	initCalendar();
	initSelectOneMenu();
}

function initWifePreferedWork() {
	$('.wifePreferedWork').selectize({
		delimiter : ',',
		persist : false,
		create : function(input) {
			return {
				value : input,
				text : input
			}
		}
	});
}